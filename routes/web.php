<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::resource('/pengguna', UserController::class);
Route::resource('/user', UserController::class);
Route::resource('/informasi', InformasiController::class);
Route::resource('/ormawa', OrmawaController::class);
Route::resource('/pendaftaran', PendaftaranController::class);
Route::resource('/pendaftaran/{pendaftaran_id}/prestasi', PrestasiController::class);
Route::resource('/pendaftaran/{pendaftaran_id}/pengalaman', PengalamanController::class);


Route::get('/pendaftaran/{id}/changestatus', 'PendaftaranController@changeStatus')->name('changestatus');

Route::get('/export/{surat}/{type}/{id}', 'PendaftaranController@exportPdf')->name('export');
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/get-status', 'HomeController@getStatus')->name('get-status');
Route::get('/get-type', 'HomeController@getType')->name('get-type');
