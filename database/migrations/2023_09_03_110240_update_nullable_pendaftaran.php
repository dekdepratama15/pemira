<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNullablePendaftaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pendaftarans', function (Blueprint $table) {
            $table->dropUnique('pendaftarans_nim_unique');
            $table->unsignedBigInteger('ormawa_id')->nullable()->change();
            $table->integer('nim')->nullable()->change();
            $table->string('type')->nullable()->change();
            $table->string('status')->nullable()->change();
            $table->string('nama_lengkap')->nullable()->change();
            $table->string('nama_panggilan')->nullable()->change();
            $table->string('prodi')->nullable()->change();
            $table->integer('semester')->nullable()->change();
            $table->string('tempat_lahir')->nullable()->change();
            $table->date('tanggal_lahir')->nullable()->change();
            $table->string('alamat')->nullable()->change();
            $table->string('kota')->nullable()->change();
            $table->string('agama')->nullable()->change();
            $table->string('no_hp')->nullable()->change();
            $table->string('hobi')->nullable()->change();
            $table->string('sakit_pernah_diderita')->nullable()->change();
            $table->text('foto')->nullable()->change();
            $table->text('catatan')->nullable()->change();
            $table->string('yang_disuka')->nullable()->change();
            $table->string('yang_tidak_disuka')->nullable()->change();
            $table->string('kebiasaan_buruk')->nullable()->change();
            $table->string('motto')->nullable()->change();
            $table->string('tujuan')->nullable()->change();
            $table->text('alasan')->nullable()->change();
            $table->string('yang_akan_dilakukan')->nullable()->change();
            $table->text('penjelasan_diri')->nullable()->change();
            $table->text('motivasi')->nullable()->change();
            $table->text('visi_misi')->nullable()->change();
            $table->string('pendidikan_terakhir')->nullable()->change();
            $table->string('pekerjaan')->nullable()->change();
            $table->string('lokasi_kerja')->nullable()->change();
            $table->string('jabatan_kerja')->nullable()->change();
            $table->date('tahun_kerja')->nullable()->change();
            $table->text('surat_dukungan')->nullable()->change();
            $table->string('orang_tua')->nullable()->change();
            $table->string('nama_orang_tua')->nullable()->change();
            $table->string('no_telp_orang_tua')->nullable()->change();
            $table->text('alamat_orang_tua')->nullable()->change();
            $table->text('krs_terakhir')->nullable()->change();
            $table->text('foto_ktm')->nullable()->change();
            $table->text('khs_terakhir')->nullable()->change();
            $table->text('sertif_kepengurusan')->nullable()->change();
            $table->text('tanda_tangan')->nullable()->change();
            $table->text('tanda_tangan_orangtua')->nullable()->change();
            $table->string('type_sebagai')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
