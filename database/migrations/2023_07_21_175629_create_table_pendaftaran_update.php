<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePendaftaranUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendaftarans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('ormawa_id');
            $table->integer('nim')->unique();
            $table->string('type');
            $table->string('status');
            $table->string('nama_lengkap');
            $table->string('nama_panggilan');
            $table->string('prodi');
            $table->integer('smester');
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->string('alamat');
            $table->string('kota');
            $table->string('agama');
            $table->integer('no_hp');
            $table->string('hobi');
            $table->string('sakit_pernah_diderita');
            $table->text('foto');
            $table->text('catatan');
            $table->string('yang_disuka');
            $table->string('yang_tidak_disuka');
            $table->string('kebiasaan_buruk');
            $table->string('motto');
            $table->string('tujuan');
            $table->text('alasan');
            $table->string('yang_akan_dilakukan');
            $table->text('penjelasan_diri');
            $table->text('motivasi');
            $table->text('visi_misi');
            $table->string('pendidikan_terakhir');
            $table->string('pekerjaan');
            $table->string('lokasi_kerja');
            $table->string('jabatan_kerja');
            $table->date('tahun_kerja');
            $table->text('surat_dukungan');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('ormawa_id')->references('id')->on('ormawas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendaftarans');
    }
}
