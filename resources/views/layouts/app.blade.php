<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Pemira</title>
    <link rel="icon" href="{{url('dist/images/logo-circle.png')}}" type="image/png">

    <!-- Styles -->
    <link href="{{ asset('dist/css/app.css') }}" rel="stylesheet">
</head>
<body @guest class="login" @else class="app" @endguest>
        @guest
        @else
            @include('layouts.nav')
        @endguest
        @yield('content')

<!-- Scripts -->
<script src="{{ asset('dist/js/app.js') }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    $(document).ready(function () {
        $('.delete-data').click(function (e) { 
            e.preventDefault();
            var id = $(this).data("id");
            var token = $(this).data("token");
            var redirect = $(this).data("redirect");
            var url_special = $(this).data("url_special");
            var url_redirect;
            if (url_special != undefined && url_special != null && url_special != '') {
                url_redirect = url_special;
            } else {
                url_redirect = '/'+redirect+'/'+id;
            }
            Swal.fire({
                title: 'Yakin?',
                text: "Data akan dihapus!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0275d8',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Hapus!',
                cancelButtonText: 'Batal'
            })
            .then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "delete",
                        url: url_redirect,
                        data: {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token,
                        },
                        success: function (response) {
                            Swal.fire(
                                'Berhasil!',
                                response.message,
                                'success'
                            )
                            .then((result) => {
                                location.reload();
                            });

                        }
                    });
                }
            })
        });
    });

    function openTab(tab){
        $.each($('.tab-menu-pendaftaran'), function (key, val) {
            if ($(val).hasClass('text-blue-600')) {
                $(val).removeClass('text-blue-600')
            }
        });
        $.each($('.tab-content'), function (key, val) {
            $(val).hide()
        });
        $('#tab-pendaftaran-' + tab).addClass('text-blue-600');
        $('#' + tab).show();
    }
</script>

@if(session('success'))
    <script>
    $(document).ready(function() {
        Swal.fire(
            'Berhasil!',
            '{{session("success")}}',
            'success'
        );
    });
    </script>
@endif


@yield('js-extra')

</body>
</html>
