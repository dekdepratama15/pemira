
<!-- BEGIN: Mobile Menu -->
<div class="mobile-menu md:hidden">
    <div class="mobile-menu-bar">
        <a href="" class="flex mr-auto">
            <img alt="" class="w-8" src="{{url('dist/images/logo-circle.png')}}">
        </a>
        <a href="javascript:;" id="mobile-menu-toggler"> <i data-feather="bar-chart-2" class="w-8 h-8 text-white transform -rotate-90"></i> </a>
    </div>
    <ul class="border-t border-theme-24 py-5 hidden">
        <li>
            <a href="{{ url('/dashboard') }}" class="menu {{ request()->is('dashboard*') ? 'menu--active' : '' }}">
                <div class="menu__icon"> <i data-feather="home"></i> </div>
                <div class="menu__title"> Dashboard </div>
            </a>
        </li>
        @if(auth()->user()->role == 'admin')
            <li>
                <a href="{{ url('/pengguna') }}" class="menu {{ request()->is('pengguna*') ? 'menu--active' : '' }}">
                    <div class="menu__icon"> <i data-feather="users"></i> </div>
                    <div class="menu__title"> Pengguna</div>
                </a>
            </li>
            <li>
                <a href="{{ url('/ormawa') }}" class="menu {{ request()->is('ormawa*') ? 'menu--active' : '' }}">
                    <div class="menu__icon"> <i data-feather="slack"></i> </div>
                    <div class="menu__title"> Ormawa</div>
                </a>
            </li>
        @endif
        <li>
            <a href="{{ url('/pendaftaran') }}" class="menu {{ request()->is('pendaftaran*') ? 'menu--active' : '' }}">
                <div class="menu__icon"> <i data-feather="clipboard"></i> </div>
                <div class="menu__title"> Pendaftaran</div>
            </a>
        </li>
        <li>
            <a href="{{ url('/informasi') }}" class="menu {{ request()->is('informasi*') ? 'menu--active' : '' }}">
                <div class="menu__icon"> <i data-feather="info"></i> </div>
                <div class="menu__title"> Informasi</div>
            </a>
        </li>
    </ul>
</div>
<!-- END: Mobile Menu -->
<!-- BEGIN: Top Bar -->
<div class="border-b border-theme-24 -mt-10 md:-mt-5 -mx-3 sm:-mx-8 px-3 sm:px-8 pt-3 md:pt-0 mb-10">
    <div class="top-bar-boxed flex items-center">
        <!-- BEGIN: Logo -->
        <a href="" class="-intro-x hidden md:flex">
            <img alt="" class="w-10" src="{{url('dist/images/logo-circle.png')}}">
            <!-- <span class="text-white text-lg ml-3"> Mid<span class="font-medium">one</span> </span> -->
        </a>
        <!-- END: Logo -->
        <!-- BEGIN: Breadcrumb -->
        <div class="-intro-x breadcrumb breadcrumb--light mr-auto"> <span class="">Pemira &nbsp;</span><span class="breadcrumb--active">ITB STIKOM BALI</span> </div>
        <!-- END: Breadcrumb -->
        <!-- BEGIN: Account Menu -->
        <div class="intro-x dropdown w-8 h-8 relative">
            <div class="dropdown-toggle w-8 h-8 rounded-full overflow-hidden shadow-lg image-fit zoom-in scale-110">
                <img alt="" src="{{!empty(auth()->user()->img_profile) ? url('/upload/'.auth()->user()->img_profile) : url('dist/images/user-default.png')}}">
            </div>
            <div class="dropdown-box mt-10 absolute w-56 top-0 right-0 z-20">
                <div class="dropdown-box__content box bg-theme-38 text-white">
                    <div class="p-4 border-b border-theme-40">
                        <div class="font-medium">{{auth()->user()->name}}</div>
                        <div class="text-xs text-theme-41">{{ucfirst(auth()->user()->role)}}</div>
                    </div>
                    <div class="p-2">
                        <a href="{{ route('user.edit',auth()->user()) }}" class="flex items-center p-2 transition duration-300 ease-in-out hover:bg-theme-1 rounded-md"> <i data-feather="lock" class="w-4 h-4 mr-2"></i> Reset Password </a>
                    </div>
                    <div class="p-2">
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="flex items-center p-2 transition duration-300 ease-in-out hover:bg-theme-1 rounded-md"> <i data-feather="toggle-right" class="w-4 h-4 mr-2"></i> Logout </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Account Menu -->
    </div>
</div>
<!-- END: Top Bar -->
<!-- BEGIN: Top Menu -->
<nav class="top-nav">
    <ul>
        <li>
            <a href="{{ url('/dashboard') }}" class="top-menu {{ request()->is('dashboard*') ? 'top-menu--active' : '' }}">
                <div class="top-menu__icon"> <i data-feather="home"></i> </div>
                <div class="top-menu__title"> Dashboard</div>
            </a>
        </li>
        @if(auth()->user()->role == 'admin')
            <li>
                <a href="{{ url('/pengguna') }}" class="top-menu {{ request()->is('pengguna*') ? 'top-menu--active' : '' }}">
                    <div class="top-menu__icon"> <i data-feather="users"></i> </div>
                    <div class="top-menu__title"> Pengguna</div>
                </a>
            </li>
            <li>
                <a href="{{ url('/ormawa') }}" class="top-menu {{ request()->is('ormawa*') ? 'top-menu--active' : '' }}">
                    <div class="top-menu__icon"> <i data-feather="slack"></i> </div>
                    <div class="top-menu__title"> Ormawa</div>
                </a>
            </li>
        @endif
        @php
            if (auth()->user()->role == 'admin') {
                $url_pendaftaran = url('/pendaftaran');
            } else {
                if (auth()->user()->pendaftaran != null) {
                    $url_pendaftaran = route('pendaftaran.edit', auth()->user()->pendaftaran->id);
                } else {
                    $url_pendaftaran = url('/pendaftaran/create');
                }
            }
        @endphp
        <li>
            <a href="{{ $url_pendaftaran }}" class="top-menu {{ request()->is('pendaftaran*') ? 'top-menu--active' : '' }}">
                <div class="top-menu__icon"> <i data-feather="clipboard"></i> </div>
                <div class="top-menu__title"> Pendaftaran</div>
            </a>
        </li>
        <li>
            <a href="{{ url('/informasi') }}" class="top-menu {{ request()->is('informasi*') ? 'top-menu--active' : '' }}">
                <div class="top-menu__icon"> <i data-feather="info"></i> </div>
                <div class="top-menu__title"> Informasi</div>
            </a>
        </li>
    </ul>
</nav>
<!-- END: Top Menu -->