@extends('layouts.app')

@section('content')
<div class="content">
        <!-- END: Top Bar -->
        <div class="intro-y flex items-center mt-8">
            <h2 class="text-lg font-medium mr-auto">
                Profile Layout
            </h2>
        </div>
        <!-- BEGIN: Profile Info -->
        <div class="intro-y box px-5 pt-5 mt-5">
            <div class="flex flex-col lg:flex-row border-b border-gray-200 pb-5 -mx-5">
                <div class="flex flex-1 px-5 items-center justify-center lg:justify-start">
                    <div class="w-20 h-20 sm:w-24 sm:h-24 flex-none lg:w-32 lg:h-32 image-fit relative">
                        <img alt="Midone Tailwind HTML Admin Template" class="rounded-full" src="{{!empty(auth()->user()->img_profile) ? url('/upload/'.auth()->user()->img_profile) : url('dist/images/user-default.png')}}">
                    </div>
                    <div class="ml-5">
                        <div class="w-24 sm:w-40 truncate sm:whitespace-normal font-medium text-lg">{{auth()->user()->name}}</div>
                        <div class="text-gray-600">Pengguna</div>
                    </div>
                </div>
                <div class="flex mt-6 lg:mt-0 items-center lg:items-start flex-1 flex-col justify-center text-gray-600 px-5 border-l border-r border-gray-200 border-t lg:border-t-0 pt-5 lg:pt-0">
                    <div class="truncate sm:whitespace-normal flex items-center"> <i data-feather="mail" class="w-4 h-4 mr-2"></i> {{auth()->user()->email}} </div>
                    <div class="truncate sm:whitespace-normal flex items-center mt-3"> <i data-feather="phone" class="w-4 h-4 mr-2"></i> {{!empty(auth()->user()->pendaftaran->no_hp) ? auth()->user()->pendaftaran->no_hp : 'Belum diisi'}} </div>
                    <div class="truncate sm:whitespace-normal flex items-center mt-3"> <i data-feather="map-pin" class="w-4 h-4 mr-2"></i> {{!empty(auth()->user()->pendaftaran->no_hp) ? auth()->user()->pendaftaran->alamat : 'Belum diisi'}} </div>
                </div>
                <div class="mt-6 lg:mt-0 flex-1 flex items-center justify-center px-5 border-t lg:border-0 border-gray-200 pt-5 lg:pt-0">
                    <div class="text-center rounded-md w-20 py-3">
                        <div class="font-semibold text-theme-1 text-lg">{{ucfirst(auth()->user()->pendaftaran->status)}}</div>
                        <div class="text-gray-600">Pendaftaran</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Profile Info -->
        <div class="tab-content mt-5">
            <div class="tab-content__pane active" id="profile">
                <div class="grid grid-cols-12 gap-6">
                    <!-- BEGIN: Latest Uploads -->
                    <div class="col-span-12 sm:col-span-6 lg:col-span-6">
                        <div class="intro-y box col-span-12 lg:col-span-6">
                            <div class="flex items-center px-5 py-5 sm:py-3 border-b border-gray-200">
                                <h2 class="font-medium text-base mr-auto">
                                    File Upload
                                </h2>
                                <div class="dropdown relative ml-auto sm:hidden">
                                    <a class="dropdown-toggle w-5 h-5 block" href="javascript:;"> <i data-feather="more-horizontal" class="w-5 h-5 text-gray-700"></i> </a>
                                    <div class="dropdown-box mt-5 absolute w-40 top-0 right-0 z-10">
                                        <div class="dropdown-box__content box p-2"> <a href="javascript:;" class="block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">All Files</a> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="p-5">
                                
                                <div class="flex items-center">
                                    <div class="file"> <a href="" class="w-12 file__icon file__icon--directory"></a> </div>
                                    <div class="ml-4">
                                        <a class="font-medium" href="">Foto 3 x 4</a> 
                                        <div class="text-{{!empty(auth()->user()->pendaftaran->foto) ? 'green' : 'red'}}-600 text-xs">{{!empty(auth()->user()->pendaftaran->foto) ? 'Sudah' : 'Belum'}} Upload</div>
                                    </div>
                                </div>
                                <div class="flex items-center mt-5">
                                    <div class="file"> <a href="" class="w-12 file__icon file__icon--empty-directory"></a> </div>
                                    <div class="ml-4">
                                        <a class="font-medium" href="">Tanda Tangan</a> 
                                        <div class="text-{{!empty(auth()->user()->pendaftaran->tanda_tangan) ? 'green' : 'red'}}-600 text-xs">{{!empty(auth()->user()->pendaftaran->tanda_tangan) ? 'Sudah' : 'Belum'}} Upload</div>
                                    </div>
                                </div>
                                <div class="flex items-center mt-5">
                                    <div class="file"> <a href="" class="w-12 file__icon file__icon--empty-directory"></a> </div>
                                    <div class="ml-4">
                                        <a class="font-medium" href="">Tanda Tangan Orang Tua</a> 
                                        <div class="text-{{!empty(auth()->user()->pendaftaran->tanda_tangan_orangtua) ? 'green' : 'red'}}-600 text-xs">{{!empty(auth()->user()->pendaftaran->tanda_tangan_orangtua) ? 'Sudah' : 'Belum'}} Upload</div>
                                    </div>
                                </div>
                                <div class="flex items-center mt-5">
                                    <div class="file"> <a href="" class="w-12 file__icon file__icon--empty-directory"></a> </div>
                                    <div class="ml-4">
                                        <a class="font-medium" href="">Surat Dukungan</a> 
                                        <div class="text-{{!empty(auth()->user()->pendaftaran->surat_dukungan) ? 'green' : 'red'}}-600 text-xs">{{!empty(auth()->user()->pendaftaran->surat_dukungan) ? 'Sudah' : 'Belum'}} Upload</div>
                                    </div>
                                </div>
                                <div class="flex items-center mt-5">
                                    <div class="file"> <a href="" class="w-12 file__icon file__icon--empty-directory"></a> </div>
                                    <div class="ml-4">
                                        <a class="font-medium" href="">KRS Terakhir</a> 
                                        <div class="text-{{!empty(auth()->user()->pendaftaran->krs_terakhir) ? 'green' : 'red'}}-600 text-xs">{{!empty(auth()->user()->pendaftaran->krs_terakhir) ? 'Sudah' : 'Belum'}} Upload</div>
                                    </div>
                                </div>
                                <div class="flex items-center mt-5">
                                    <div class="file"> <a href="" class="w-12 file__icon file__icon--empty-directory"></a> </div>
                                    <div class="ml-4">
                                        <a class="font-medium" href="">KHS Terakhir</a> 
                                        <div class="text-{{!empty(auth()->user()->pendaftaran->khs_terakhir) ? 'green' : 'red'}}-600 text-xs">{{!empty(auth()->user()->pendaftaran->khs_terakhir) ? 'Sudah' : 'Belum'}} Upload</div>
                                    </div>
                                </div>
                                <div class="flex items-center mt-5">
                                    <div class="file"> <a href="" class="w-12 file__icon file__icon--empty-directory"></a> </div>
                                    <div class="ml-4">
                                        <a class="font-medium" href="">Sertifikat Kepengurusan Ormawa</a> 
                                        <div class="text-{{!empty(auth()->user()->pendaftaran->sertif_kepengurusan) ? 'green' : 'red'}}-600 text-xs">{{!empty(auth()->user()->pendaftaran->sertif_kepengurusan) ? 'Sudah' : 'Belum'}} Upload</div>
                                    </div>
                                </div>
                                <div class="flex items-center mt-5">
                                    <div class="file"> <a href="" class="w-12 file__icon file__icon--empty-directory"></a> </div>
                                    <div class="ml-4">
                                        <a class="font-medium" href="">Foto Kartu Tanda Mahasiswa</a> 
                                        <div class="text-{{!empty(auth()->user()->pendaftaran->foto_ktm) ? 'green' : 'red'}}-600 text-xs">{{!empty(auth()->user()->pendaftaran->foto_ktm) ? 'Sudah' : 'Belum'}} Upload</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END: Latest Uploads -->
                    </div>

                    <div class="col-span-12 sm:col-span-6 lg:col-span-6">
                        <!-- BEGIN: New Products -->
                        <div class="intro-y box col-span-6">
                            <div class="flex items-center px-5 py-3 border-b border-gray-200">
                                <h2 class="font-medium text-base mr-auto">
                                    Informasi
                                </h2>
                                <button data-carousel="new-products" data-target="prev" class="slick-navigator button px-2 border text-white relative flex items-center text-gray-700 mr-2"> <i data-feather="chevron-left" class="w-4 h-4"></i> </button>
                                <button data-carousel="new-products" data-target="next" class="slick-navigator button px-2 border text-white relative flex items-center text-gray-700"> <i data-feather="chevron-right" class="w-4 h-4"></i> </button>
                            </div>
                            <div class="slick-carousel py-5" id="new-products">
                                @foreach($data['informasi'] as $informasi)
                                <div class="px-5">
                                    <div class="flex flex-col lg:flex-row items-center pb-5">
                                        <div class="flex flex-col sm:flex-row items-center pr-5 lg:border-r border-gray-200">
                                            <div class="sm:mr-5">
                                                <div class="w-20 h-20 image-fit">
                                                    <img  class="rounded-full" src="{{url('dist/images/megaphone.png')}}">
                                                </div>
                                            </div>
                                            <div class="mr-auto text-center sm:text-left mt-3 sm:mt-0">
                                                <a href="" class="font-medium text-lg">{{$informasi->judul}}</a> 
                                                <div class="text-gray-600 mt-1 sm:mt-0">{{$informasi->deskripsi}}</div>
                                            </div>
                                        </div>
                                        <div class="w-full lg:w-auto mt-6 lg:mt-0 pt-4 lg:pt-0 flex-1 flex items-center justify-center px-5 border-t lg:border-t-0 border-gray-200">
                                            <button class="button w-20 justify-center block bg-gray-200 text-gray-600 ml-auto">Download</button>
                                        </div>
                                    </div>
                                    <div class="flex flex-col sm:flex-row items-center border-t border-gray-200 pt-5">
                                        <div class="w-full sm:w-auto flex justify-center sm:justify-start items-center border-b sm:border-b-0 border-gray-200 pb-5 sm:pb-0">
                                            <div class="px-3 py-2 bg-theme-14 text-theme-10 rounded font-medium mr-3">{{date('d M Y', strtotime($informasi->created_at))}}</div>
                                            <div class="text-gray-600">Date of Release</div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- END: New Products -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection