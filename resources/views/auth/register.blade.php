@extends('layouts.app')

@section('content')
<div class="container sm:px-10">
    <div class="block xl:grid grid-cols-2 gap-4">
        <!-- BEGIN: Login Info -->
        <div class="hidden xl:flex flex-col min-h-screen">
            <a href="" class="-intro-x flex items-center pt-5">
                <span class="text-white text-lg ml-3"> Pemira<span class="font-medium"> ITB STIKOM BALI</span> </span>
            </a>
            <div class="my-auto">
                <img alt="Midone Tailwind HTML Admin Template" class="-intro-x w-1/2 -mt-16" src="dist/images/logo-circle.png">
                <div class="-intro-x text-white font-medium text-4xl leading-tight mt-10">
                    Sistem Informasi  
                    <br>
                    Pendaftaran Bakal Calon
                </div>
                <div class="-intro-x mt-5 text-lg text-white">Pemilihan Umum Raya ITB STIKOM BALI</div>
            </div>
        </div>
        <!-- END: Login Info -->
        <!-- BEGIN: Login Form -->
        <div class="h-screen xl:h-auto flex py-5 xl:py-0 my-10 xl:my-0">
            <div class="my-auto mx-auto xl:ml-20 bg-white xl:bg-transparent px-5 sm:px-8 py-8 xl:p-0 rounded-md shadow-md xl:shadow-none w-full sm:w-3/4 lg:w-2/4 xl:w-auto">
                <h2 class="intro-x font-bold text-2xl xl:text-3xl text-center xl:text-left">
                    Register
                </h2>

                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="intro-x mt-8">
                        <input type="text" class="intro-x login__input input input--lg border border-gray-300 @error('name') border-theme-12 @enderror block" placeholder="Username" name="name" value="{{ old('name') }}" required>
                        @error('name')
                            <div class="text-theme-12 mt-2">{{ $message }}</div>
                        @enderror
                        <input type="email" class="intro-x login__input input input--lg border border-gray-300 @error('email') border-theme-12 @enderror block mt-4" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email">
                        @error('email')
                            <div class="text-theme-12 mt-2">{{ $message }}</div>
                        @enderror
                        <input type="password" class="intro-x login__input input input--lg border border-gray-300 @error('password') border-theme-12 @enderror block mt-4" placeholder="Password" name="password" required autocomplete="new-password">
                        @error('password')
                            <div class="text-theme-12 mt-2">{{ $message }}</div>
                        @enderror
                        <input type="password" class="intro-x login__input input input--lg border border-gray-300 block mt-4" placeholder="Confirm Password" name="password_confirmation" required autocomplete="new-password">
                    </div>
                    <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                        <button class="button button--lg w-full xl:w-32 text-white bg-theme-1 xl:mr-3">Register</button>
                        <a type="button" href="/login" class="button button--lg w-full xl:w-32 text-gray-700 border border-gray-300 mt-3 xl:mt-0">Login</a>
                    </div>
                </form>
            </div>
        </div>
        <!-- END: Login Form -->
    </div>
</div>
@endsection
