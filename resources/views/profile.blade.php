@extends('layouts.app')

@section('content')
    <div class="content" >
        <div class="content">
            <div class="intro-y flex items-center mt-8">
                <h2 class="text-lg font-medium mr-auto">
                    Update Profile
                </h2>
            </div>
            <div class="grid grid-cols-12 gap-6">
                <!-- BEGIN: Profile Menu -->
                <div class="col-span-12 lg:col-span-4 xxl:col-span-3 flex lg:block flex-col-reverse">
                    <div class="intro-y box mt-5">
                        <div class="p-5 border-t border-gray-200">
                            <a class="flex items-center text-theme-1 font-medium" href=""> <i data-feather="user" class="w-4 h-4 mr-2"></i> Account Setting </a>
                        </div>
                    </div>
                </div>
                <!-- END: Profile Menu -->
                <div class="col-span-12 lg:col-span-8 xxl:col-span-9">
                    <!-- BEGIN: Display Information -->
                    <div class="intro-y box lg:mt-5">
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Account Information
                            </h2>
                        </div>
                        <form method="post" action="{{route('user.update',auth()->user()->id)}}" enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="p-5">
                                <div class="grid grid-cols-12 gap-5">
                                    <div class="col-span-12 xl:col-span-4">
                                        <div class="border border-gray-200 rounded-md p-5">
                                            <div class="w-40 h-40 relative image-fit cursor-pointer zoom-in mx-auto" id="imagePrev">
                                                <img class="rounded-md" alt="Masukkan Foto Profile" src="{{url('upload/'.auth()->user()->img_profile)}}">
                                                <div title="Foto Profile" class="tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2"> <i data-feather="x" class="w-4 h-4"></i> </div>
                                            </div>
                                            <div class="w-40 mx-auto cursor-pointer relative mt-5">
                                                <button type="button" class="button w-full bg-theme-1 text-white">Change Photo</button>
                                                <input type="file" class="w-full h-full top-0 left-0 absolute opacity-0" name="imgProfile" id="imageInput">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-span-12 xl:col-span-8">
                                        <div>
                                            <label>Name</label>
                                            <input type="text" class="input w-full border bg-gray-100 cursor-not-allowed mt-2" placeholder="Input text" value="{{auth()->user()->name}}" disabled>
                                        </div>
                                        <div class="mt-3">
                                            <div>
                                                <label>Email</label>
                                                <input type="text" class="input w-full border bg-gray-100 cursor-not-allowed mt-2" placeholder="Input text" value="{{auth()->user()->email}}" disabled>
                                            </div>
                                        </div>
                                        <div class="mt-3">
                                            <div>
                                                <label>Password</label>
                                                <input type="password" class="input w-full border bg-white mt-2" placeholder="New Password" value="" name="password" >
                                            </div>
                                        </div>
                                        <div class="mt-3">
                                            <div>
                                                <label>Confirm Password</label>
                                                <input type="password" class="input w-full border bg-white mt-2" placeholder="Confirm New Password" value="" name="confirmpassword" >
                                            </div>
                                        </div>
                                        {{-- <a href="{{url('/pengguna')}}" class="button h-20 bg-gray-700 text-white mt-3 mr-2">Kembali</a>
                                        <button type="submit" class="button w-20 h-9 bg-theme-1 text-white mt-3">Save</button> --}}
                                        <div class="col-span-6 mt-3">
                                            <div class="flex justify-start">
                                                <a href="{{url('/pengguna')}}" class="button text-white bg-gray-600 shadow-md mr-2">Kembali</a>
                                                <button type="submit" class="button text-white bg-theme-1 shadow-md mr-2">Simpan</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </form>
                    </div>
                    <!-- END: Display Information -->
                </div>
            </div>
        </div>
    </div>

    <script>
        const imageInput = document.getElementById('imageInput');
        const imagePreview = document.getElementById('imagePrev');

        imageInput.addEventListener('change', function(event) {
            const file = event.target.files[0];
            
            if (file) {
                const reader = new FileReader();
                
                reader.onload = function(e) {
                    const img = document.createElement('img');
                    img.src = e.target.result;
                    imagePreview.innerHTML = '';
                    imagePreview.appendChild(img);
                }
                
                reader.readAsDataURL(file);
            }
        }); 
    </script>
@endsection