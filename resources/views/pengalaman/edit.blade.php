@extends('layouts.app')
@section('content')
    <div class="content">
        <div class="grid grid-cols-12 gap-6">
            <!-- BEGIN: Profile Menu -->
            <!-- END: Profile Menu -->
            <form action="/pendaftaran/{{$pendaftaran_id}}/pengalaman/{{$pengalaman->id}}" method="post" enctype="multipart/form-data" class="col-span-12 lg:col-span-12 xxl:col-span-12">
                @csrf
                @method('PUT')
                <div class="col-span-12 lg:col-span-12 xxl:col-span-12" >
                    <div class="intro-y box lg:mt-5 tab-content" id="pengalaman">
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Ubah Pengalaman
                            </h2>
                        </div>
                        <div id="div-pengalaman">
                            <div class="p-5">
                                <div class="grid grid-cols-12 gap-5">
                                    <div class="col-span-12 xl:col-span-6">
                                        <div class="">
                                            <label>Type</label>
                                            <select name="type_pengalaman" class="input w-full border mt-2" required>
                                                <option value="">-Pilih Type-</option>
                                                <option value="Organisasi" {{ $pengalaman->type == 'Organisasi' ? 'selected' : '' }}>Organisasi</option>
                                                <option value="Kepanitiaan" {{ $pengalaman->type == 'Kepanitiaan' ? 'selected' : '' }}>Kepanitiaan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-span-12 xl:col-span-6">
                                        <div class="">
                                            <label>Jabatan</label>
                                            <input type="text" name="jabatan_pengalaman" class="input w-full border mt-2" placeholder="Jabatan" value="{{ $pengalaman->jabatan }}" required />
                                            @if($errors->any() && $errors->get('jabatan_pengalaman') != null)
                                                <div class="mt-1" >
                                                    <i class="text-red-600 text-sm">
                                                        *Field Wajib Dilengkapi
                                                    </i>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-span-12 xl:col-span-12">
                                        <div class="">
                                            <label>Pengalaman</label>
                                            <textarea type="text" name="deskripsi_pengalaman" class="input w-full border mt-2" placeholder="Kegiatan">{{ $pengalaman->kegiatan }}</textarea>
                                            @if($errors->any() && $errors->get('deskripsi_pengalaman') != null)
                                                <div class="mt-1" >
                                                    <i class="text-red-600 text-sm">
                                                        *Field Wajib Dilengkapi
                                                    </i>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="flex justify-end gap-2">
                    <div class="flex justify-end mt-4">
                        <a href="{{url('/pendaftaran/'.$pendaftaran_id.'/edit')}}" type="submit" class="button w-20  bg-gray-600 text-white ml-auto">Kembali</a>
                    </div>
                    <div class="flex justify-end mt-4">
                        <button type="submit" class="button w-20 bg-theme-1 text-white ml-auto">Simpan</button>
                    </div>
                </div>
               
            </form>
        </div>
    </div>
@endsection