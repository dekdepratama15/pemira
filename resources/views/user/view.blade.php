@extends('layouts.app')

@section('content')
<div class="content">
    <h2 class="intro-y text-lg font-medium mt-10">
        View Pengguna
    </h2>
    <div class="grid grid-cols-12 gap-6">
        <div class="col-span-12 lg:col-span-12 xxl:col-span-12">
            <!-- BEGIN: Display Information -->
            <div class="intro-y box lg:mt-5">
                <div class="flex items-center p-5 border-b border-gray-200">
                    <h2 class="font-medium text-base mr-auto">
                        Display Information
                    </h2>
                </div>
                <div class="p-5">
                    <div class="grid grid-cols-12 gap-5">
                        <div class="col-span-12 xl:col-span-4">
                            <div class="border border-gray-200 rounded-md p-5">
                                <div class="w-40 h-40 relative image-fit cursor-pointer zoom-in mx-auto">
                                    <img class="rounded-md" alt="Midone Tailwind HTML Admin Template" src="{{!empty($user->img_profile) ? url('upload/'.$user->img_profile) : url('dist/images/user-default.png')}}">
                                    <!-- <div title="Remove this profile photo?" class="tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2"> <i data-feather="x" class="w-4 h-4"></i> </div> -->
                                </div>
                                <div class="w-40 mx-auto cursor-pointer relative mt-5">
                                    <!-- <button type="button" class="button w-full bg-theme-1 text-white">Change Photo</button> -->
                                    <input type="file" class="w-full h-full top-0 left-0 absolute opacity-0">
                                </div>
                            </div>
                        </div>
                        <div class="col-span-12 xl:col-span-8">
                            <div>
                                <label>Username</label>
                                <input type="text" class="input w-full border bg-gray-200 cursor-not-allowed mt-2" placeholder="Input text" value="{{$user->name}}" disabled>
                            </div>
                            <div class="mt-3">
                                <label>Email</label>
                                <input type="text" class="input w-full border bg-gray-200 cursor-not-allowed mt-2" placeholder="Input text" value="{{$user->email}}" disabled>
                            </div>
                            <div class="mt-3">
                                <label>Register pada</label>
                                <input type="text" class="input w-full border bg-gray-200 cursor-not-allowed mt-2" placeholder="Input text" value="{{date('d M Y', strtotime($user->created_at))}}" disabled>
                            </div>
                            <a href="/pengguna" type="button" class="button w-20 bg-theme-1 text-white mt-3">Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: Display Information -->
        </div>
    </div>
</div>
@endsection