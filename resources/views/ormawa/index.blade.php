@extends('layouts.app')

@section('content')
<div class="content">
    <h2 class="intro-y text-lg font-medium mt-10">
        Data List Ormawa
    </h2>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
            <a href="{{url('/ormawa/create')}}" class="button text-white bg-theme-1 shadow-md mr-2">Tambah Data</a>
            <div class="hidden md:block mx-auto text-gray-600"></div>
            <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
                
                <div class="w-56 relative text-gray-700">
                    <form action="" method="get">
                        <input type="text" name="search" value="{{$search}}" class="input w-56 box pr-10 placeholder-theme-13" placeholder="Search...">
                        <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i> 
                    </form>
                </div>
            </div>
        </div>
        <!-- BEGIN: Data List -->
        <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
            <table class="table table-report -mt-2">
                <thead>
                    <tr>
                        <th class="whitespace-no-wrap">NAMA ORMAWA</th>
                        <th class="text-center flex justify-end items-center whitespace-no-wrap mr-20">ACTIONS</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($ormawas) > 0)
                        @foreach($ormawas as $ormawa)
                            <tr class="intro-x">
                                <td class="w-40">
                                    <h1 class="text-base" >{{$ormawa->ormawa}}</h1>
                                </td>
                                <td class="table-report__action w-56">
                                    <div class="flex justify-end items-center">
                                        <a class="flex items-center button button--sm text-white bg-theme-1 p-2" href="{{ route('ormawa.show',$ormawa->id) }}"> <i data-feather="eye" class="w-4 h-4 mr-1"></i> View </a>
                                        <a class="flex items-center button button--sm text-white bg-theme-12 ml-1 p-2" href="{{ route('ormawa.edit',$ormawa->id) }}"> <i data-feather="edit" class="w-4 h-4 mr-1"></i> Edit </a>
                                        <button class="flex items-center button button--sm text-white bg-theme-6 p-2 m-1 delete-data" type="button" data-redirect="ormawa" data-id="{{ $ormawa->id }}" data-token="{{ csrf_token() }}"> <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="border-b whitespace-no-wrap text-center" colspan="2">Tidak terdapat data ormawa</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <!-- END: Data List -->
        <!-- BEGIN: Pagination -->
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-no-wrap items-center">
            {{-- {{ $users->links('vendor.pagination.default') }}     --}}
        </div>
        <!-- END: Pagination -->
    </div>
</div>
@endsection