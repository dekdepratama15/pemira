@extends('layouts.app')

@section('content')
<div class="content">
    <h2 class="intro-y text-lg font-medium mt-10">
        Tambah Ormawa
    </h2>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
            <div class="hidden md:block mx-auto text-gray-600"></div>
            <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            </div>
        </div>
        <!-- BEGIN: Data List -->
        <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <form action="/ormawa" method="post">
            @csrf
            <div class="flex flex-col" >
                <label for="ormawa" class="block text-sm font-medium leading-6 text-gray-900">Nama Ormawa</label>
                    <div class="mt-2">
                        <input id="ormawa" name="ormawa" value="{{$ormawa->ormawa}}" type="ormawa" placeholder="Masukkan Nama Ormawa" autocomplete="ormawa" class="block w-1/2 rounded-md border-0 py-1.5 px-2 text-gray-700 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 h-10 bg-gray-200" readonly>
                    </div>
                </div>
            </div>
            <div class="col-span-6">
                <div class="flex justify-start">
                    <a href="{{url('/ormawa')}}" class="button text-white bg-gray-600 shadow-md mr-2">Kembali</a>
                </div>
            </div>
        </form>
</div>
@endsection