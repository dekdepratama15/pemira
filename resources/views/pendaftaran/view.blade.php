@extends('layouts.app')
@section('content')
    <div class="content">
        <div class="grid grid-cols-12 gap-6">
            <!-- BEGIN: Profile Menu -->
            <div class="col-span-12 lg:col-span-4 xxl:col-span-3 flex lg:block flex-col-reverse">
                <div class="intro-y box mt-5">

                    <div class="p-5 border-t border-gray-200" >
                        <li class="flex items-center cursor-pointer hover:text-blue-500 text-blue-600 tab-menu-pendaftaran" id="tab-pendaftaran-personal" href="" onclick="openTab('personal')"> <i data-feather="user" class="w-4 h-4 mr-2"></i> Data Pribadi </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-perkuliahan" onclick="openTab('perkuliahan')" > <i data-feather="layers" class="w-4 h-4 mr-2"></i> Perkuliahan </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-pekerjaan" onclick="openTab('pekerjaan')" > <i data-feather="sliders" class="w-4 h-4 mr-2"></i> Pekerjaan </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-riwayat" onclick="openTab('riwayat')" > <i data-feather="archive" class="w-4 h-4 mr-2"></i> Riwayat </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-prestasi" onclick="openTab('prestasi')" > <i data-feather="star" class="w-4 h-4 mr-2"></i> Prestasi </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-pengalaman" onclick="openTab('pengalaman')" > <i data-feather="github" class="w-4 h-4 mr-2"></i> Pengalaman </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-suratpendukung" onclick="openTab('suratpendukung')" > <i data-feather="inbox" class="w-4 h-4 mr-2"></i> Surat Pendukung </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-lainnya" onclick="openTab('lainnya')" > <i data-feather="more-vertical" class="w-4 h-4 mr-2"></i> Lainnya </li>
                    </div>
                </div>
            </div>
            <!-- END: Profile Menu -->
            <form action="" method="post" enctype="multipart/form-data" class="col-span-12 lg:col-span-8 xxl:col-span-9">
                @csrf
                <div class="col-span-12 lg:col-span-8 xxl:col-span-9" >
                    <!-- BEGIN: Personal Information -->
                    <div class="intro-y box lg:mt-5 tab-content" id="personal" style="display: block"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Data Diri
                            </h2>
                        </div>
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 xl:col-span-6">
                                    <div>
                                        <label>Nama</label>
                                        <input type="text" name="" class="input w-full border mt-2 bg-gray-200 cursor-default" placeholder="" value="{{$pendaftaran->user->name}}" readonly >
                                    </div>
                                    <div class="mt-3">
                                        <label>Nama Lengkap</label>
                                        <input type="text" name="nama_lengkap"  class="input w-full border mt-2 bg-gray-200 cursor-default" placeholder="Masukkan Nama Lengkap" value="{{$pendaftaran->nama_lengkap}}" readonly >
                                    </div>
                                    <div class="mt-3">
                                        <label>Tempat Lahir</label>
                                        <input type="text" name="tempat_lahir" class="input w-full border mt-2 bg-gray-200 cursor-default" placeholder="Masukkan Tempat Lahir" value="{{$pendaftaran->tempat_lahir}}" readonly >
                                    </div>
                                    <div class="mt-4">
                                        <label>Kota</label>
                                        <input type="text" name="kota" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Nama Kota" value="{{$pendaftaran->kota}}">
                                    </div>
                                    <div class="mt-3">
                                        <label>No Hp</label>
                                        <input type="text" name="no_hp" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan No Handphone" value="{{$pendaftaran->no_hp}}">
                                    </div>
                                    <div class="mt-4">
                                        <label>Ormawa</label>
                                        <input type="text" name="ormawa" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan No Handphone" value="{{$pendaftaran->ormawa->ormawa}}">
                                    </div>
                                    <div class="mt-3">
                                        <label>Orang Tua</label>
                                        <input type="text" name="orang_tua" class="input w-full border mt-2 bg-gray-200 cursor-default" placeholder="Masukkan Tempat Lahir" value="{{$pendaftaran->orang_tua}}" readonly >
                                    </div>
                                    <div class="mt-3">
                                        <label>Alamat Orang Tua</label>
                                        <input type="text" name="alamat_orang_tua" class="input w-full border mt-2 bg-gray-200 cursor-default" placeholder="Masukkan Alamat Orang Tua" value="{{$pendaftaran->alamat_orang_tua}}" readonly>
                                    </div>
                                    <div class="mt-3">
                                        <label>Tanda Tangan Orang Tua</label>
                                        {{-- <input type="file" name="surat_dukungan" class="input w-full border mt-2" placeholder="" value="" > --}}
                                        <a class="flex items-center button w-12 mt-2 button--sm text-white bg-theme-1 h-10 " href="{{ url('upload/'.$pendaftaran->tanda_tangan_orangtua) }}" target="_blank"> Lihat </a>
                                        {{-- <i class="mt-2" >Isikan jika ingin mengupload ulang file</i> --}}
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-6">
                                    <div>
                                        <label>Email</label>
                                        <input type="text" name="" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="" value="{{$pendaftaran->user->email}}">
                                    </div>
                                    <div class="mt-3">
                                        <label>Nama Panggilan</label>
                                        <input type="text" name="nama_panggilan" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Nama Panggilan" value="{{$pendaftaran->nama_panggilan}}">
                                    </div>
                                    <div class="mt-3">
                                        <label>Tanggal Lahir</label>
                                        <input type="date" name="tanggal_lahir" name="nama_panggilan" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Input text" value="{{$pendaftaran->tanggal_lahir}}">
                                    </div>
                                    <div class="mt-4">
                                        <label>Agama</label>
                                        <input type="text" name="agama" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Nama Agama" value="{{$pendaftaran->agama}}">
                                    </div>
                                    <div class="mt-3">
                                        <label>Hobi</label>
                                        <input type="text" name="hobi" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Nama Hobi" value="{{$pendaftaran->hobi}}">
                                    </div>
                                    <div class="mt-3">
                                        <label>Nama Orang Tua</label>
                                        <input type="text" name="nama_orang_tua" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Nama Orang Tua" value="{{$pendaftaran->nama_orang_tua}}">
                                    </div>
                                    <div class="mt-3">
                                        <label>No Telp Orang Tua</label>
                                        <input type="text" name="no_telp_orang_tua" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan No Telp Orang Tua" value="{{$pendaftaran->no_telp_orang_tua}}">
                                    </div>
                                    <div class="mt-3">
                                        <label>Tanda Tangan</label>
                                        {{-- <input type="file" name="surat_dukungan" class="input w-full border mt-2" placeholder="" value="" > --}}
                                        <a class="flex items-center button w-12 mt-2 button--sm text-white bg-theme-1 h-10 " href="{{ url('upload/'.$pendaftaran->tanda_tangan) }}" target="_blank"> Lihat </a>
                                        {{-- <i class="mt-2" >Isikan jika ingin mengupload ulang file</i> --}}
                                    </div>
                                    
                                </div>
                                <div class="col-span-12" >
                                    <div class="">
                                        <label>Alamat</label>
                                        <textarea type="text" name="alamat" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Nama Alamat" value="">{{$pendaftaran->alamat}}</textarea>
                                    </div>
                                    <div>
                                        <label>Foto</label>
                                        <img class="w-64 h-36" src="{{ url('upload/'.$pendaftaran->foto) }}" alt="">
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <!-- END: Personal Information -->

                    {{-- Perkuliahan --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="perkuliahan" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Perkuliahan
                            </h2>
                        </div>
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 xl:col-span-6">
                                    <div>
                                        <label>Nim</label>
                                        <input type="text" name="nim" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Nim" value="{{$pendaftaran->nim}}" >
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-6">
                                    <div>
                                        <label>Prodi</label>
                                        <input type="text" name="prodi" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Nama Prodi" value="{{$pendaftaran->prodi}}">
                                    </div>
                                </div>
                                <div class="col-span-12 ">
                                    <div>
                                        <label>Semester</label>
                                        <input type="text" name="semester" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Nama Semester" value="{{$pendaftaran->semester}}">
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-12 flex gap-6">
                                    <div>
                                        <label>KRS Terakhir</label>
                                        {{-- <input type="file" name="surat_dukungan" class="input w-full border mt-2" placeholder="" value="" > --}}
                                        <a class="flex items-center button w-12 mt-2 button--sm text-white bg-theme-1 h-10 " href="{{ url('upload/'.$pendaftaran->krs_terakhir) }}" target="_blank"> Lihat </a>
                                        {{-- <i class="mt-2" >Isikan jika ingin mengupload ulang file</i> --}}
                                    </div>
                                    <div>
                                        <label>KHS Terakhir</label>
                                        {{-- <input type="file" name="surat_dukungan" class="input w-full border mt-2" placeholder="" value="" > --}}
                                        <a class="flex items-center button w-12 mt-2 button--sm text-white bg-theme-1 h-10 " href="{{ url('upload/'.$pendaftaran->khs_terakhir) }}" target="_blank"> Lihat </a>
                                        {{-- <i class="mt-2" >Isikan jika ingin mengupload ulang file</i> --}}
                                    </div>
                                    <div>
                                        <label>Foto KTM</label>
                                        {{-- <input type="file" name="surat_dukungan" class="input w-full border mt-2" placeholder="" value="" > --}}
                                        <a class="flex items-center button w-12 mt-2 button--sm text-white bg-theme-1 h-10 " href="{{ url('upload/'.$pendaftaran->foto_ktm) }}" target="_blank"> Lihat </a>
                                        {{-- <i class="mt-2" >Isikan jika ingin mengupload ulang file</i> --}}
                                    </div>
                                    <div>
                                        <label>Sertifikat Kegiatan atau Kepengurusan Ormawa</label>
                                        {{-- <input type="file" name="surat_dukungan" class="input w-full border mt-2" placeholder="" value="" > --}}
                                        <a class="flex items-center button w-12 mt-2 button--sm text-white bg-theme-1 h-10 " href="{{ url('upload/'.$pendaftaran->sertif_kepengurusan) }}" target="_blank"> Lihat </a>
                                        {{-- <i class="mt-2" >Isikan jika ingin mengupload ulang file</i> --}}
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    {{-- End perkuliahan --}}

                    {{-- Pekerjaan --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="pekerjaan" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Pekerjaan
                            </h2>
                        </div>
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 xl:col-span-6">
                                    <div>
                                        <label>Pekerjaan</label>
                                        <input type="text" name="pekerjaan" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Nama Pekerjaan" value="{{$pendaftaran->pekerjaan}}" >
                                    </div>
                                    <div class="mt-3">
                                        <label>Jabatan Kerja</label>
                                        <input type="text" name="jabatan_pekerjaan" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Jabatan Pekerjaan" value="{{$pendaftaran->jabatan_kerja}}" >
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-6">
                                    <div>
                                        <label>Lokasi Kerja</label>
                                        <input type="text" name="lokasi_kerja" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Lokasi Pekerjaan" value="{{$pendaftaran->lokasi_kerja}}">
                                    </div>
                                    <div class="mt-3">
                                        <label>Tahun Kerja</label>
                                        <input type="date" name="tahun_kerja" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Tahun Kerja" value="{{$pendaftaran->tahun_kerja}}">
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    {{-- End Pekerjaan --}}

                    {{-- Riwayat --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="riwayat" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Riwayat
                            </h2>
                        </div>
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12">
                                    <div>
                                        <label>Sakit Yang Pernah Diderita</label>
                                        <input type="text" name="sakit_pernah_diderita" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Sakit Yang Pernah Diderita" value="{{$pendaftaran->sakit_pernah_diderita}}" >
                                    </div>
                                    <div class="mt-3">
                                        <label>Pendidikan Terakhir / Asal Sekolah</label>
                                        <input type="text" name="pendidikan_terakhir" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Pendidikan Terakhir" value="{{$pendaftaran->pendidikan_terakhir}}" >
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    {{-- End Riwayat --}}

                    {{-- Surat pendukung --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="suratpendukung" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Surat Pendukung
                            </h2>
                        </div>
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 ">
                                    <div>
                                        <label>Surat Pendukung</label>
                                        {{-- <input type="file" name="surat_dukungan" class="input w-full border mt-2" placeholder="" value="" > --}}
                                        <a class="flex items-center button w-12 mt-2 button--sm text-white bg-theme-1 h-10 " href="{{ url('upload/'.$pendaftaran->surat_dukungan) }}" target="_blank"> Lihat </a>
                                        {{-- <i class="mt-2" >Isikan jika ingin mengupload ulang file</i> --}}
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    {{-- End Surat pendukung --}}

                    {{-- Lainnya --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="lainnya" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Lainnya
                            </h2>
                        </div>
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 xl:col-span-6">
                                    <div class="">
                                        <label>Type</label>
                                        <input type="text" name="yang_disuka" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Hal Yang Disukai" value="{{$pendaftaran->type}}" >
                                    </div>
                                    <div class="mt-10">
                                        <label>Yang Disuka</label>
                                        <textarea type="text" name="yang_disuka" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Hal Yang Disukai" value="" >{{$pendaftaran->yang_disuka}}</textarea>
                                    </div>
                                    <div class="mt-3">
                                        <label>Kebiasaan Buruk</label>
                                        <textarea type="text" name="kebiasaan_buruk" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Kebiasaan Buruk Yang Sering Dilakukan" value="" >{{$pendaftaran->kebiasaan_buruk}}</textarea>
                                    </div>
                                    <div class="mt-3">
                                        <label>Tujuan</label>
                                        <textarea type="text" name="tujuan" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Tujuan Mendaftarkan Diri Menjadi Bem/Balma/Hima" value="" >{{$pendaftaran->tujuan}}</textarea>
                                    </div>
                                    <div class="mt-3">
                                        <label>Yang Akan Dilakukan</label>
                                        <textarea type="text" name="yang_akan_dilakukan" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Yang Akan Dilakukan Jika Menjadi Bem/Balma/Hima" value="">{{$pendaftaran->yang_akan_dilakukan}}</textarea>
                                    </div>
                                    <div class="mt-3">
                                        <label>Motivasi</label>
                                        <textarea type="text" name="motivasi" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Motivasi Kamu Menjadi Bem/Balma/Hima" value="">{{$pendaftaran->motivasi}}</textarea>
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-6">
                                    <div class="">
                                        <label>Sebagai</label>
                                        <input type="text" name="yang_disuka" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Hal Yang Disukai" value="{{$pendaftaran->type_sebagai}}" >
                                    </div>
                                    <div>
                                        <label>Yang Tidak Disuka</label>
                                        <textarea type="text" name="yang_tidak_disuka" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Hal Yang Tidak Disukai" value="">{{$pendaftaran->yang_tidak_disuka}}</textarea>
                                    </div>
                                    <div class="mt-3">
                                        <label>Motto</label>
                                        <textarea type="text" name="motto" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Motto Kamu" value="">{{$pendaftaran->motto}}</textarea>
                                    </div>
                                    <div class="mt-3">
                                        <label>Alasan</label>
                                        <textarea type="text" name="alasan" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Alasan Kamu Ingin Menjadi Bem/Balma/Hima" value="">{{$pendaftaran->alasan}}</textarea>
                                    </div>
                                    <div class="mt-3">
                                        <label>Penjelasan Diri</label>
                                        <textarea type="text" name="penjelasan_diri" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Deskripsikan Diri Kamu Ingin Menjadi Bem/Balma/Hima" value="">{{$pendaftaran->penjelasan_diri}}</textarea>
                                    </div>
                                    <div class="mt-3">
                                        <label>Visi Misi</label>
                                        <textarea type="text" name="visi_misi" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly placeholder="Masukkan Visi Misi Kamu" value="">{{$pendaftaran->visi_misi}}</textarea>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    {{-- End Lainnya --}}

                    {{-- Prestasi --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="prestasi" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Prestasi
                            </h2>
                        </div>
                        <div class="p-5">
                            @foreach ($pendaftaran->prestasis as $prestasi)
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 xl:col-span-6">
                                    <div class="">
                                        <label>Type</label>
                                        <input type="text" name="jabatan_pengalaman" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly  placeholder="Jabatan" value="{{$prestasi->type}}" />   
                                       
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-6">
                                    <div class="">
                                        <label>Prestasi</label>
                                        <textarea type="text" name="deskripsi_prestasi" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly  placeholder="Deskripsi Prestasi" value="" >{{$prestasi->prestasi}}</textarea>                                           
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    {{-- End Prestasi --}}

                    {{-- Pengalaman --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="pengalaman" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Pengalaman
                            </h2>
                        </div>
                        <div class="p-5">
                            @foreach ($pendaftaran->pengalamans as $pengalaman)
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 xl:col-span-6">
                                    <div class="">
                                        <label>Type</label>
                                        <input type="text" name="jabatan_pengalaman" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly  placeholder="Jabatan" value="{{$pengalaman->type}}" />
                                            
                                       
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-6">
                                    <div class="">
                                        <label>Jabatan</label>
                                        <input type="text" name="jabatan_pengalaman" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly  placeholder="Jabatan" value="{{$pengalaman->jabatan}}" />
                                            
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-12">
                                    <div class="">
                                        <label>Prestasi</label>
                                        <textarea type="text" name="deskripsi_pengalaman" class="input w-full border mt-2 bg-gray-200 cursor-default" readonly  placeholder="Jabatan" value="" >{{$pengalaman->kegiatan}}</textarea>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    {{-- End Prestasi --}}
                </div>
                <div class="flex justify-end gap-2">
                    <div class="flex justify-end mt-4">
                        <a href="{{url('/pendaftaran')}}" type="submit" class="button w-20  bg-gray-600 text-white ml-auto">Kembali</a>
                    </div>
                    {{-- <div class="flex justify-end mt-4">
                        <button type="submit" class="button w-20 bg-theme-1 text-white ml-auto">Simpan</button>
                    </div> --}}
                </div>
               
            </form>
        </div>
    </div>
@endsection