@extends('layouts.app')

@section('content')
<div class="content">
    <h2 class="intro-y text-lg font-medium mt-10">
        Data List Pendaftaran
    </h2>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
            <div class="hidden md:block mx-auto text-gray-600"></div>
            <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
                
                <div class="w-56 relative text-gray-700">
                    <form action="" method="get">
                        <input type="text" name="search" value="{{$search}}" class="input w-56 box pr-10 placeholder-theme-13" placeholder="Search...">
                        <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i> 
                    </form>
                </div>
            </div>
        </div>
        <!-- BEGIN: Data List -->
        <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
            <table class="table table-report -mt-2">
                <thead>
                    <tr>
                        <th class="whitespace-no-wrap">PENDAFTAR</th>
                        <th class="whitespace-no-wrap" >SEBAGAI</th>
                        <th class="whitespace-no-wrap" >STATUS</th>
                        <th class="text-center flex justify-end items-center whitespace-no-wrap mr-20">ACTIONS</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($pendaftarans) > 0)
                        @foreach($pendaftarans as $pendaftaran)
                            <tr class="intro-x">
                                <td class="w-40">
                                    <a href="{{ route('pendaftaran.show',$pendaftaran->id) }}" class="font-medium whitespace-no-wrap">{{$pendaftaran->user->name}}</a> 
                                    <div class="text-gray-600 text-xs whitespace-no-wrap">{{$pendaftaran->user->email}}</div>
                                </td>
                                <td class="w-20">
                                    <h1 class="text-base" >{{$pendaftaran->type_sebagai}} {{$pendaftaran->type}}</h1>
                                </td>
                                <td class="w-20">
                                    @php
                                        $badge_color = $pendaftaran->status == 'pending' ? 'gray' : (
                                            $pendaftaran->status == 'submit' || $pendaftaran->status == 'resubmit' ? 'blue' : (
                                                $pendaftaran->status == 'unverified' ? 'yellow' : (
                                                    $pendaftaran->status == 'verified' ? 'green' : 'red'
                                                )
                                            )
                                        );
                                    @endphp
                                    <span class="bg-{{$badge_color}}-100 text-{{$badge_color}}-800 text-xs font-medium mr-2 px-3 py-1 rounded dark:bg-gray-700 dark:text-{{$badge_color}}-400 border border-{{$badge_color}}-400">{{ucfirst($pendaftaran->status)}}</span>
                                </td>
                                <td class="table-report__action w-56">
                                    <div class="flex justify-end items-center">
                                        <a class="flex items-center button button--sm text-white bg-theme-1 p-2" href="{{ route('pendaftaran.edit',$pendaftaran->id) }}"> <i data-feather="eye" class="w-4 h-4 mr-1"></i> View </a>
                                        <!-- <a class="flex items-center button button--sm text-white bg-theme-12 ml-1 p-2" href="{{ route('pendaftaran.edit',$pendaftaran->id) }}"> <i data-feather="edit" class="w-4 h-4 mr-1"></i> Edit </a> -->
                                        <!-- <button class="flex items-center button button--sm text-white bg-theme-6 p-2 m-1 delete-data" type="button" data-redirect="pendaftaran" data-id="{{ $pendaftaran->id }}" data-token="{{ csrf_token() }}"> <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete </button> -->
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="border-b whitespace-no-wrap text-center" colspan="4">Tidak terdapat data pendaftaran</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <!-- END: Data List -->
        <!-- BEGIN: Pagination -->
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-no-wrap items-center">
            {{-- {{ $users->links('vendor.pagination.default') }}     --}}
        </div>
        <!-- END: Pagination -->
    </div>
</div>
@endsection