@extends('layouts.app')
@section('content')
    <style>
        .bg-gray-200{
            bg-opacity: 1 !important;
            background-color: #edf2f7 !important;
            background-color: rgba(237, 242, 247, var(--bg-opacity)) !important;
        }
    </style>
    <div class="content">
        <div class="grid grid-cols-12 gap-6">
            <!-- BEGIN: Profile Menu -->
            <div class="col-span-12 lg:col-span-4 xxl:col-span-3 flex lg:block flex-col-reverse">
                <div class="intro-y box mt-5">

                    <div class="p-5 border-t border-gray-200" >
                        <li class="flex items-center cursor-pointer hover:text-blue-500 text-blue-600 tab-menu-pendaftaran" id="tab-pendaftaran-personal" href="" onclick="openTab('personal')"> <i data-feather="user" class="w-4 h-4 mr-2"></i> Data Pribadi </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-perkuliahan" onclick="openTab('perkuliahan')" > <i data-feather="layers" class="w-4 h-4 mr-2"></i> Perkuliahan </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-pekerjaan" onclick="openTab('pekerjaan')" > <i data-feather="sliders" class="w-4 h-4 mr-2"></i> Pekerjaan </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-riwayat" onclick="openTab('riwayat')" > <i data-feather="archive" class="w-4 h-4 mr-2"></i> Riwayat </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-prestasi" onclick="openTab('prestasi')" > <i data-feather="star" class="w-4 h-4 mr-2"></i> Prestasi </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-pengalaman" onclick="openTab('pengalaman')" > <i data-feather="github" class="w-4 h-4 mr-2"></i> Pengalaman </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-suratpendukung" onclick="openTab('suratpendukung')" > <i data-feather="inbox" class="w-4 h-4 mr-2"></i> Surat </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-lainnya" onclick="openTab('lainnya')" > <i data-feather="more-vertical" class="w-4 h-4 mr-2"></i> Lainnya </li>
                    </div>
                </div>
            </div>
            <!-- END: Profile Menu -->
            <form action="{{route('pendaftaran.update',$pendaftaran->id)}}" method="post" enctype="multipart/form-data" class="col-span-12 lg:col-span-8 xxl:col-span-9">
                @method('PUT')
                @csrf
                <div class="col-span-12 lg:col-span-8 xxl:col-span-9" >
                    <div class="bg-{{$alert['color']}}-100 border-t-4 border-{{$alert['color']}}-500 rounded-b text-{{$alert['color']}}-900 px-4 py-3 shadow-md intro-y box lg:mt-5" role="alert">
                        <div class="flex">
                            <div class="py-1"><i data-feather="{{$alert['icon']}}" class="mr-2"></i> </div>
                            <div>
                            <p class="font-bold">{{$alert['title']}}</p>
                            <p class="text-sm">{{$alert['message']}}</p>
                            @if(isset($alert['useCatatan']) && $alert['useCatatan'])
                            <p class="font-bold">Catatan Admin</p>
                            <p class="text-sm">{{$pendaftaran->catatan}}</p>
                            @endif
                            </div>
                        </div>
                    </div>
                    <!-- BEGIN: Personal Information -->
                    <div class="intro-y box lg:mt-5 tab-content" id="personal" style="display: block"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Data Diri
                            </h2>
                        </div>
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 xl:col-span-6">
                                    <div>
                                        <label>Username</label>
                                        <input type="text" name="" class="input w-full border bg-gray-200 mt-2" placeholder="" value="{{$pendaftaran->user->name}}" >
                                    </div>
                                    <div class="mt-3">
                                        <label>Nama Lengkap</label>
                                        <input type="text" name="nama_lengkap"  class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan Nama Lengkap" value="{{!empty($pendaftaran->nama_lengkap) ? $pendaftaran->nama_lengkap : old('nama_lengkap')}}" {{auth()->user()->role == 'admin' ? 'readonly' : ''}}>
                                        @if($errors->any() && $errors->get('nama_lengkap') != null)
                                        <div class="mt-1" >
                                            <i class="text-red-600 text-sm">
                                                *Field Wajib Dilengkapi
                                            </i>
                                        </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Tempat Lahir</label>
                                        <input type="text" name="tempat_lahir" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan Tempat Lahir" value="{{!empty($pendaftaran->tempat_lahir) ? $pendaftaran->tempat_lahir : old('tempat_lahir')}}" {{auth()->user()->role == 'admin' ? 'readonly' : ''}} >
                                        @if($errors->any() && $errors->get('tempat_lahir') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-4">
                                        <label>Kota</label>
                                        <input type="text" name="kota" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan Nama Kota" value="{{!empty($pendaftaran->kota) ? $pendaftaran->kota : old('kota')}}" {{auth()->user()->role == 'admin' ? 'readonly' : ''}}>
                                        @if($errors->any() && $errors->get('kota') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>No Hp</label>
                                        <input type="text" name="no_hp" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan No Handphone" value="{{!empty($pendaftaran->no_hp) ? $pendaftaran->no_hp : old('no_hp')}}" {{auth()->user()->role == 'admin' ? 'readonly' : ''}}>
                                        @if($errors->any() && $errors->get('no_hp') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Ormawa</label>
                                        <select name="ormawa_id" class="input w-full border mt-3 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" {{auth()->user()->role == 'admin' ? 'disabled' : ''}}>
                                            <option value="">Pilih Ormawa</option>
                                            @foreach ($ormawas as $ormawa)
                                                <option value="{{$ormawa->id}}" {{$pendaftaran->ormawa_id == $ormawa->id ? 'selected' : ''}} >{{!empty($ormawa->ormawa) ? $ormawa->ormawa : old('ormawa')}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mt-4">
                                        <label>Orang Tua</label>
                                        <select name="orang_tua" class="input w-full border mt-3 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" {{auth()->user()->role == 'admin' ? 'disabled' : ''}}>
                                            <option value="Ayah" {{$pendaftaran->orang_tua == 'Ayah' ? 'selected' : ''}} >Ayah</option>
                                            <option value="Ibu" {{$pendaftaran->orang_tua == 'Ibu' ? 'selected' : ''}} >Ibu</option>
                                            <option value="Wali" {{$pendaftaran->orang_tua == 'Wali' ? 'selected' : ''}} >Wali</option>
                                        </select>
                                    </div>
                                    <div class="mt-3">
                                        <label>Alamat Orang Tua</label>
                                        <input type="text" name="alamat_orang_tua" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan Alamat Orang Tua" value="{{!empty($pendaftaran->alamat_orang_tua) ? $pendaftaran->alamat_orang_tua : old('alamat_orang_tua')}}" {{auth()->user()->role == 'admin' ? 'readonly' : ''}}>
                                        @if($errors->any() && $errors->get('alamat_orang_tua') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Tanda Tangan Orang Tua</label>
                                        <div class="relative mt-2"> 
                                            <input type="file" name="tanda_tangan_orangtua" class="input pr-12 w-full border col-span-4 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Price" {{auth()->user()->role == 'admin' ? 'disabled' : ''}}>
                                            @if(!empty($pendaftaran->tanda_tangan_orangtua))
                                                <a class="absolute top-0 right-0 rounded-r w-10 h-full flex items-center justify-center bg-theme-1 border text-white" href="{{ url('upload/'.$pendaftaran->tanda_tangan_orangtua) }}" target="_blank" >Lihat</a>
                                            @endif
                                        </div>
                                        @if(!empty($pendaftaran->tanda_tangan_orangtua) && auth()->user()->role != 'admin')
                                            <i>isikan jika ingin mengubah file</i>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-6">
                                    <div>
                                        <label>Email</label>
                                        <input type="text" name="" class="input w-full border bg-gray-200 mt-2" placeholder="" value="{{$pendaftaran->user->email}}">
                                    </div>
                                    <div class="mt-3">
                                        <label>Nama Panggilan</label>
                                        <input type="text" name="nama_panggilan" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan Nama Panggilan" value="{{!empty($pendaftaran->nama_panggilan) ? $pendaftaran->nama_panggilan : old('nama_panggilan')}}" {{auth()->user()->role == 'admin' ? 'readonly' : ''}}>
                                        @if($errors->any() && $errors->get('nama_panggilan') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Tanggal Lahir</label>
                                        <input type="date" name="tanggal_lahir" name="nama_panggilan" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Input text" value="{{!empty($pendaftaran->tanggal_lahir) ? $pendaftaran->tanggal_lahir : old('tanggal_lahir')}}" {{auth()->user()->role == 'admin' ? 'readonly' : ''}}>
                                        @if($errors->any() && $errors->get('tanggal_lahir') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-4">
                                        <label>Alamat</label>
                                        <input type="text" name="alamat" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan Nama Alamat" value="{{!empty($pendaftaran->alamat) ? $pendaftaran->alamat : old('alamat')}}" {{auth()->user()->role == 'admin' ? 'readonly' : ''}}>
                                        @if($errors->any() && $errors->get('alamat') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-2">
                                        <label>Agama</label>
                                        <select name="agama" class="input w-full border mt-3 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" {{auth()->user()->role == 'admin' ? 'disabled' : ''}}>
                                            <option value="Ayah" {{$pendaftaran->agama == 'Hindu' ? 'selected' : ''}} >Hindu</option>
                                            <option value="Ibu" {{$pendaftaran->agama == 'Budha' ? 'selected' : ''}} >Budha</option>
                                            <option value="Islam" {{$pendaftaran->agama == 'Islam' ? 'selected' : ''}} >Islam</option>
                                            <option value="Kristen Katolik" {{$pendaftaran->agama == 'Kristen Katolik' ? 'selected' : ''}} >Kristen Katolik</option>
                                            <option value="Kristen Protestan" {{$pendaftaran->agama == 'Kristen Protestan' ? 'selected' : ''}} >Kristen Protestan</option>
                                            <option value="Budha" {{$pendaftaran->agama == 'Budha' ? 'selected' : ''}} >Budha</option>
                                            <option value="Konghucu" {{$pendaftaran->agama == 'Konghucu' ? 'selected' : ''}} >Konghucu</option>
                                            <option value="Lainnya" {{$pendaftaran->agama == 'Lainnya' ? 'selected' : ''}} >Lainnya</option>
                                        </select>
                                    </div>
                                    <div class="mt-3">
                                        <label>Hobi</label>
                                        <input type="text" name="hobi" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan Nama Hobi" value="{{!empty($pendaftaran->hobi) ? $pendaftaran->hobi : old('hobi')}}" {{auth()->user()->role == 'admin' ? 'readonly' : ''}}>
                                        @if($errors->any() && $errors->get('hobi') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Nama Orang Tua</label>
                                        <input type="text" name="nama_orang_tua" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan Nama Orang Tua" value="{{!empty($pendaftaran->nama_orang_tua) ? $pendaftaran->nama_orang_tua : old('nama_orang_tua')}}" {{auth()->user()->role == 'admin' ? 'readonly' : ''}}>
                                        @if($errors->any() && $errors->get('nama_orang_tua') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-4">
                                        <label>No Telp Orang Tua</label>
                                        <input type="text" name="no_telp_orang_tua" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan No Telp Orang Tua" value="{{!empty($pendaftaran->no_telp_orang_tua) ? $pendaftaran->no_telp_orang_tua : old('no_telp_orang_tua')}}" {{auth()->user()->role == 'admin' ? 'readonly' : ''}}>
                                        @if($errors->any() && $errors->get('no_telp_orang_tua') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Foto</label>
                                        <div class="relative mt-2"> 
                                            <input type="file" name="foto" class="input pr-12 w-full border col-span-4 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Price" {{auth()->user()->role == 'admin' ? 'disabled' : ''}}>
                                            @if(!empty($pendaftaran->foto))
                                                <a class="absolute top-0 right-0 rounded-r w-10 h-full flex items-center justify-center bg-theme-1 border text-white" href="{{ url('upload/'.$pendaftaran->foto) }}" target="_blank" >Lihat</a>
                                            @endif
                                        </div>
                                        @if(!empty($pendaftaran->foto) && auth()->user()->role != 'admin')
                                            <i>isikan jika ingin mengubah file</i>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-span-12" >
                                    <div class="">
                                        <label>Alamat</label>
                                        <textarea type="text" name="alamat" name="nama_panggilan" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan Alamat" value="" {{auth()->user()->role == 'admin' ? 'readonly' : ''}}>{{!empty($pendaftaran->alamat) ? $pendaftaran->alamat : old('alamat')}}</textarea>
                                        @if($errors->any() && $errors->get('alamat') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <!-- END: Personal Information -->

                    {{-- Perkuliahan --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="perkuliahan" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Perkuliahan
                            </h2>
                        </div>
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 xl:col-span-6">
                                    <div>
                                        <label>Nim</label>
                                        <input type="text" name="nim" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan Nim" value="{{!empty($pendaftaran->nim) ? $pendaftaran->nim : old('nim')}}" {{auth()->user()->role == 'admin' ? 'readonly' : ''}} >
                                        @if($errors->any() && $errors->get('nim') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-6">
                                    <div>
                                        <label>Prodi</label>
                                        <select name="prodi" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" {{auth()->user()->role == 'admin' ? 'disabled' : ''}}>
                                            <option value="">Pilih Prodi</option>
                                            <option value="Sistem Informasi" {{$pendaftaran->prodi == 'Sistem Informasi' ? 'selected' : ''}} >Sistem Informasi</option>
                                            <option value="Sistem Komputer" {{$pendaftaran->prodi == 'Sistem Komputer' ? 'selected' : ''}} >Sistem Komputer</option>
                                            <option value="Teknologi Informasi" {{$pendaftaran->prodi == 'Teknologi Informasi' ? 'selected' : ''}} >Teknologi Informasi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-span-12 ">
                                    <div>
                                        <label>Semester</label>
                                        <input type="text" name="semester" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan Nama Semester" value="{{!empty($pendaftaran->semester) ? $pendaftaran->semester : old('semester')}}" {{auth()->user()->role == 'admin' ? 'readonly' : ''}}>
                                        @if($errors->any() && $errors->get('semester') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-span-12" >
                                    <div>
                                        <label>KRS TERAKHIR</label>
                                        <div class="relative mt-2"> 
                                            <input type="file" name="krs_terakhir" class="input pr-12 w-full border col-span-4 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" {{auth()->user()->role == 'admin' ? 'disabled' : ''}}>
                                            @if(!empty($pendaftaran->krs_terakhir))
                                                <a class="absolute top-0 right-0 rounded-r w-10 h-full flex items-center justify-center bg-theme-1 border text-white" href="{{ url('upload/'.$pendaftaran->krs_terakhir) }}" target="_blank" >Lihat</a>
                                            @endif
                                        </div>
                                        @if(!empty($pendaftaran->krs_terakhir) && auth()->user()->role != 'admin')
                                            <i>isikan jika ingin mengubah file</i>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>KHS TERAKHIR</label>
                                        <div class="relative mt-2"> <input type="file" name="khs_terakhir" class="input pr-12 w-full border col-span-4 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" {{auth()->user()->role == 'admin' ? 'disabled' : ''}}>
                                            @if(!empty($pendaftaran->khs_terakhir))
                                                <a class="absolute top-0 right-0 rounded-r w-10 h-full flex items-center justify-center bg-theme-1 border text-white" href="{{ url('upload/'.$pendaftaran->khs_terakhir) }}" target="_blank" >Lihat</a>
                                            @endif
                                        </div>
                                        @if(!empty($pendaftaran->khs_terakhir) && auth()->user()->role != 'admin')
                                            <i>isikan jika ingin mengubah file</i>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Sertifikat Kegiatan atau Kepengurusan Ormawa</label>
                                        <div class="relative mt-2"> <input type="file" name="sertif_kepengurusan" class="input pr-12 w-full border col-span-4 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" {{auth()->user()->role == 'admin' ? 'disabled' : ''}}>
                                            @if(!empty($pendaftaran->sertif_kepengurusan))
                                                <a class="absolute top-0 right-0 rounded-r w-10 h-full flex items-center justify-center bg-theme-1 border text-white" href="{{ url('upload/'.$pendaftaran->sertif_kepengurusan) }}" target="_blank" >Lihat</a>
                                            @endif
                                        </div>
                                        @if(!empty($pendaftaran->sertif_kepengurusan) && auth()->user()->role != 'admin')
                                            <i>isikan jika ingin mengubah file</i>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Foto KTM</label>
                                        <div class="relative mt-2"> <input type="file" name="foto_ktm" class="input pr-12 w-full border col-span-4 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" {{auth()->user()->role == 'admin' ? 'disabled' : ''}}>
                                            @if(!empty($pendaftaran->foto_ktm))
                                                <a class="absolute top-0 right-0 rounded-r w-10 h-full flex items-center justify-center bg-theme-1 border text-white" href="{{ url('upload/'.$pendaftaran->foto_ktm) }}" target="_blank" >Lihat</a>
                                            @endif
                                        </div>
                                        @if(!empty($pendaftaran->foto_ktm) && auth()->user()->role != 'admin')
                                            <i>isikan jika ingin mengubah file</i>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    {{-- End perkuliahan --}}

                    {{-- Pekerjaan --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="pekerjaan" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <div>
                                <h2 class="font-medium text-base mr-auto">
                                    Pekerjaan
                                </h2>
                            </div>
                            <div class="w-full sm:w-auto flex items-center sm:ml-auto mt-3 sm:mt-0">
                                <div class="mr-3">Tidak sedang bekerja</div>
                                <input data-target="#input_pekerjaan" class="show-code input input--switch border input-toggle" data-type="pekerjaan" id="input_pekerjaan" type="checkbox" value="{{$pendaftaran->memilikiPekerjaan == 0 ? 1:0}}" {{$pendaftaran->memilikiPekerjaan == 0 ? 'checked':''}} {{auth()->user()->role == 'admin' ? 'disabled' : ''}}>
                                <input type="hidden" id="value_pekerjaan" name="checkbox_pekerjaan" value="{{$pendaftaran->memilikiPekerjaan == 0 ? 1:0}}">
                            </div>
                        </div>
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 xl:col-span-6">
                                    <div>
                                        <label>Pekerjaan</label>
                                        <input type="text" name="pekerjaan" class="input w-full border mt-2 input-pekerjaan {{$pendaftaran->memilikiPekerjaan == 0 || auth()->user()->role == 'admin' ? 'bg-gray-200':''}}" placeholder="Masukkan Nama Pekerjaan" value="{{!empty($pendaftaran->pekerjaan) ? $pendaftaran->pekerjaan : old('pekerjaan')}}" {{$pendaftaran->memilikiPekerjaan == 0 || auth()->user()->role == 'admin'? 'readonly':''}} >
                                    </div>
                                    <div class="mt-3">
                                        <label>Jabatan Kerja</label>
                                        <input type="text" name="jabatan_pekerjaan" class="input w-full border mt-2 input-pekerjaan {{$pendaftaran->memilikiPekerjaan == 0 || auth()->user()->role == 'admin' ? 'bg-gray-200':''}}" placeholder="Masukkan Jabatan Pekerjaan" value="{{!empty($pendaftaran->jabatan_kerja) ? $pendaftaran->jabatan_kerja : old('jabatan_kerja')}}" {{$pendaftaran->memilikiPekerjaan == 0 || auth()->user()->role == 'admin'? 'readonly':''}} >
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-6">
                                    <div>
                                        <label>Lokasi Kerja</label>
                                        <input type="text" name="lokasi_kerja" class="input w-full border mt-2 input-pekerjaan {{$pendaftaran->memilikiPekerjaan == 0 || auth()->user()->role == 'admin' ? 'bg-gray-200':''}}" placeholder="Masukkan Lokasi Pekerjaan" value="{{!empty($pendaftaran->lokasi_kerja) ? $pendaftaran->lokasi_kerja : old('lokasi_kerja')}}" {{$pendaftaran->memilikiPekerjaan == 0 || auth()->user()->role == 'admin'? 'readonly':''}} >
                                    </div>
                                    <div class="mt-3">
                                        <label>Tahun Kerja</label>
                                        <input type="date" name="tahun_kerja" class="input w-full border mt-2 input-pekerjaan {{$pendaftaran->memilikiPekerjaan == 0 || auth()->user()->role == 'admin' ? 'bg-gray-200':''}}" placeholder="Masukkan Tahun Kerja" value="{{!empty($pendaftaran->tahun_kerja) ? $pendaftaran->tahun_kerja : old('tahun_kerja')}}" {{$pendaftaran->memilikiPekerjaan == 0 || auth()->user()->role == 'admin'? 'readonly':''}} >
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    {{-- End Pekerjaan --}}

                    {{-- Riwayat --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="riwayat" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Riwayat
                            </h2>
                        </div>
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12">
                                    <div>
                                        <div class="flex">
                                            <label>Sakit Yang Pernah Diderita</label>
                                            <div class="w-full sm:w-auto flex items-center sm:ml-auto mt-3 sm:mt-0">
                                                <div class="mr-3">Tidak memiliki riwayat penyakit</div>
                                                <input data-target="#input_riwayat" class="show-code input input--switch border input-toggle" id="input_riwayat" data-type="riwayat" type="checkbox" value="{{$pendaftaran->memilikiRiwayat == 0 ? 1:0}}" {{$pendaftaran->memilikiRiwayat == 0 ? 'checked':''}} {{auth()->user()->role == 'admin' ? 'disabled' : ''}}>
                                                <input type="hidden" id="value_riwayat" name="checkbox_riwayat" value="{{$pendaftaran->memilikiRiwayat == 0 ? 1:0}}">
                                            </div>
                                        </div>
                                        <input type="text" name="sakit_pernah_diderita" class="input w-full border mt-2 input-riwayat {{$pendaftaran->memilikiRiwayat == 0 || auth()->user()->role == 'admin' ? 'bg-gray-200':''}}" placeholder="Masukkan Sakit Yang Pernah Diderita" value="{{$pendaftaran->sakit_pernah_diderita}}" {{$pendaftaran->memilikiRiwayat == 0 || auth()->user()->role == 'admin' ? 'readonly':''}}>
                                    </div>
                                    <div class="mt-3">
                                        <label>Pendidikan Terakhir / Asal Sekolah</label>
                                        <input type="text" name="pendidikan_terakhir" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan Pendidikan Terakhir" value="{{!empty($pendaftaran->pendidikan_terakhir) ? $pendaftaran->pendidikan_terakhir : old('pendidikan_terakhir')}}" {{auth()->user()->role == 'admin' ? 'readonly' : ''}} >
                                        @if($errors->any() && $errors->get('pendidikan_terakhir') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    {{-- End Riwayat --}}

                    {{-- Surat pendukung --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="suratpendukung" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Surat
                            </h2>
                        </div>
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 ">
                                    <div class="">
                                        <label>Surat Pendukung</label>
                                        <div class="relative mt-2"> <input type="file" class="input pr-12 w-full border col-span-4 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Price" name="surat_dukungan" {{auth()->user()->role == 'admin' ? 'disabled' : ''}}>
                                            @if(!empty($pendaftaran->surat_dukungan))
                                                <a class="absolute top-0 right-0 rounded-r w-10 h-full flex items-center justify-center bg-theme-1 border text-white" href="{{ url('upload/'.$pendaftaran->surat_dukungan) }}" target="_blank" >Lihat</a>
                                            @endif
                                        </div>
                                        @if(!empty($pendaftaran->surat_dukungan) && auth()->user()->role != 'admin')
                                            <i>isikan jika ingin mengubah file</i>
                                        @endif
                                    </div>
                                </div>
                                @if($pendaftaran->status == 'verified')
                                    <div class="col-span-6">
                                        <div class="">
                                            <label>Form Pendaftaran</label> <br>
                                            <a type="button" href="{{url('/export/form/'.strtolower($pendaftaran->type).'/'.$pendaftaran->id)}}" class="button w-20 bg-theme-1 text-white"> Download</a>
                                        </div>
                                    </div>
                                    <div class="col-span-6">
                                        <div class="">
                                            <label>Persetujuan Orang Tua</label> <br>
                                            <a type="button" href="{{url('/export/izin/'.strtolower($pendaftaran->type).'/'.$pendaftaran->id)}}" class="button w-20 bg-theme-1 text-white"> Download</a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            
                        </div>
                    </div>
                    {{-- End Surat pendukung --}}

                    {{-- Lainnya --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="lainnya" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Lainnya
                            </h2>
                        </div>
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 xl:col-span-6">
                                    <div class="">
                                        <label>Type</label>
                                        <select name="type" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" {{auth()->user()->role == 'admin' ? 'disabled' : ''}}>
                                            <option value="">-Pilih Type-</option>
                                            <option value="Bem" {{$pendaftaran->type == 'Bem' ? 'selected' : ''}}>Bem</option>
                                            <option value="Balma" {{$pendaftaran->type == 'Balma' ? 'selected' : ''}}>Balma</option>
                                            <option value="Hima" {{$pendaftaran->type == 'Hima' ? 'selected' : ''}}>Hima</option>
                                        </select>
                                    </div>
                                    <div class="mt-10">
                                        <label>Yang Disuka</label>
                                        <textarea type="text" name="yang_disuka" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan Hal Yang Disukai" value="" {{auth()->user()->role == 'admin' ? 'readonly' : ''}} >{{!empty($pendaftaran->yang_disuka) ? $pendaftaran->yang_disuka : old('yang_disuka')}}</textarea>
                                        @if($errors->any() && $errors->get('yang_disuka') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Kebiasaan Buruk</label>
                                        <textarea type="text" name="kebiasaan_buruk" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan Kebiasaan Buruk Yang Sering Dilakukan" value="" {{auth()->user()->role == 'admin' ? 'readonly' : ''}} >{{!empty($pendaftaran->kebiasaan_buruk) ? $pendaftaran->kebiasaan_buruk : old('kebiasaan_buruk')}}</textarea>
                                        @if($errors->any() && $errors->get('kebiasaan_buruk') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Tujuan</label>
                                        <textarea type="text" name="tujuan" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Tujuan Mendaftarkan Diri Menjadi Bem/Balma/Hima" value="" {{auth()->user()->role == 'admin' ? 'readonly' : ''}}>{{!empty($pendaftaran->tujuan) ? $pendaftaran->tujuan : old('tujuan')}}</textarea>
                                        @if($errors->any() && $errors->get('tujuan') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Yang Akan Dilakukan</label>
                                        <textarea type="text" name="yang_akan_dilakukan" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Yang Akan Dilakukan Jika Menjadi Bem/Balma/Hima" value="" {{auth()->user()->role == 'admin' ? 'readonly' : ''}}>{{!empty($pendaftaran->yang_akan_dilakukan) ? $pendaftaran->yang_akan_dilakukan : old('yang_akan_dilakukan')}}</textarea>
                                        @if($errors->any() && $errors->get('yang_akan_dilakukan') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Motivasi</label>
                                        <textarea type="text" name="motivasi" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Motivasi Kamu Menjadi Bem/Balma/Hima" value="" {{auth()->user()->role == 'admin' ? 'readonly' : ''}}>{{!empty($pendaftaran->motivasi) ? $pendaftaran->motivasi : old('motivasi')}}</textarea>
                                        @if($errors->any() && $errors->get('motivasi') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-6">
                                    <div class="">
                                        <label>Sebagai</label>
                                        <select name="type_sebagai" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" {{auth()->user()->role == 'admin' ? 'disabled' : ''}}>
                                            <option value="">-Pilih Sebagai-</option>
                                            <option value="Ketua" {{$pendaftaran->type_sebagai == 'Ketua' ?'selected' : ''}} >Ketua</option>
                                            <option value="Wakil Ketua"{{$pendaftaran->type_sebagai == 'Wakil Ketua' ?'selected' : ''}} >Wakil Ketua</option>
                                            <option value="Anggota"{{$pendaftaran->type_sebagai == 'Anggota' ?'selected' : ''}} >Anggota</option>
                                        </select>
                                    </div>
                                    <div class="mt-10" >
                                        <label>Yang Tidak Disuka</label>
                                        <textarea type="text" name="yang_tidak_disuka" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan Hal Yang Tidak Disukai" value="" {{auth()->user()->role == 'readonly' ? 'bg-gray-200' : ''}}>{{!empty($pendaftaran->yang_tidak_disuka) ? $pendaftaran->yang_tidak_disuka : old('yang_tidak_disuka')}}</textarea>
                                        @if($errors->any() && $errors->get('yang_tidak_disuka') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Motto</label>
                                        <textarea type="text" name="motto" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan Motto Kamu" value="" {{auth()->user()->role == 'readonly' ? 'bg-gray-200' : ''}}>{{!empty($pendaftaran->motto) ? $pendaftaran->motto : old('motto')}}</textarea>
                                        @if($errors->any() && $errors->get('motto') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Alasan</label>
                                        <textarea type="text" name="alasan" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Alasan Kamu Ingin Menjadi Bem/Balma/Hima" value="" {{auth()->user()->role == 'readonly' ? 'bg-gray-200' : ''}}>{{!empty($pendaftaran->alasan) ? $pendaftaran->alasan : old('alasan')}}</textarea>
                                        @if($errors->any() && $errors->get('alasan') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Penjelasan Diri</label>
                                        <textarea type="text" name="penjelasan_diri" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Deskripsikan Diri Kamu Ingin Menjadi Bem/Balma/Hima" value="" {{auth()->user()->role == 'readonly' ? 'bg-gray-200' : ''}}>{{!empty($pendaftaran->penjelasan_diri) ? $pendaftaran->penjelasan_diri : old('penjelasan_diri')}}</textarea>
                                        @if($errors->any() && $errors->get('penjelasan_diri') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Visi Misi</label>
                                        <textarea type="text" name="visi_misi" class="input w-full border mt-2 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" placeholder="Masukkan Visi Misi Kamu" value="" {{auth()->user()->role == 'readonly' ? 'bg-gray-200' : ''}}>{{!empty($pendaftaran->visi_misi) ? $pendaftaran->visi_misi : old('visi_misi')}}</textarea>
                                        @if($errors->any() && $errors->get('visi_misi') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Tanda Tangan</label>
                                        <div class="relative mt-2"> <input type="file" name="tanda_tangan" class="input pr-12 w-full border col-span-4 {{auth()->user()->role == 'admin' ? 'bg-gray-200' : ''}}" {{auth()->user()->role == 'admin' ? 'disabled' : ''}}>
                                            @if(!empty($pendaftaran->tanda_tangan))
                                                <a class="absolute top-0 right-0 rounded-r w-10 h-full flex items-center justify-center bg-theme-1 border text-white" href="{{ url('upload/'.$pendaftaran->tanda_tangan) }}" target="_blank" >Lihat</a>
                                            @endif
                                        </div>
                                        @if(!empty($pendaftaran->tanda_tangan) && auth()->user()->role != 'admin')
                                            <i>isikan jika ingin mengubah file</i>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    {{-- End Lainnya --}}

                    {{-- Prestasi --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="prestasi" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Prestasi
                            </h2>
                            @if(auth()->user()->role != 'admin')
                                <a type="button" href="{{url('/pendaftaran/'.$pendaftaran->id.'/prestasi/create')}}" class="button w-20 bg-theme-12 text-white ml-auto">Tambah</a>
                            @endif
                        </div>
                        <div class="p-5">
                            <div class="overflow-x-auto">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="border-b-2 whitespace-no-wrap">#</th>
                                            <th class="border-b-2 whitespace-no-wrap">Type</th>
                                            <th class="border-b-2 whitespace-no-wrap">Prestasi</th>
                                            <th class="border-b-2 whitespace-no-wrap text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($pendaftaran->prestasis) > 0)
                                            @foreach($pendaftaran->prestasis as $prestasi)
                                                <tr>
                                                    <td class="border-b whitespace-no-wrap">{{ $loop->iteration }}</td>
                                                    <td class="border-b whitespace-no-wrap">{{ $prestasi->type }}</td>
                                                    <td class="border-b whitespace-no-wrap">{{ $prestasi->prestasi }}</td>
                                                    <td class="border-b whitespace-no-wrap flex justify-end items-center">
                                                        @if(auth()->user()->role != 'admin')
                                                            <a type="button" class="flex items-center button button--sm text-white bg-theme-12 ml-1 p-2 w-25" href="{{url('/pendaftaran/'.$pendaftaran->id.'/prestasi/'.$prestasi->id.'/edit')}}"> <i data-feather="edit" class="w-4 h-4 mr-1"></i> Edit </a>
                                                            <button class="flex items-center button button--sm text-white bg-theme-6 p-2 m-1 delete-data" type="button" data-redirect="prestasi" data-id="{{ $prestasi->id }}" data-token="{{ csrf_token() }}" data-url_special="{{'/pendaftaran/'.$pendaftaran->id.'/prestasi/'.$prestasi->id}}"> <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete </button>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td class="border-b whitespace-no-wrap text-center" colspan="4">Tidak terdapat data prestasi</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- End Prestasi --}}

                    {{-- Pengalaman --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="pengalaman" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Pengalaman
                            </h2>
                            @if(auth()->user()->role != 'admin')
                                <a type="button" href="{{url('/pendaftaran/'.$pendaftaran->id.'/pengalaman/create')}}" class="button w-20 bg-theme-12 text-white ml-auto">Tambah</a>
                            @endif
                        </div>
                        <div class="p-5">
                            <div class="overflow-x-auto">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="border-b-2 whitespace-no-wrap">#</th>
                                            <th class="border-b-2 whitespace-no-wrap">Type</th>
                                            <th class="border-b-2 whitespace-no-wrap">Jabatan</th>
                                            <th class="border-b-2 whitespace-no-wrap">Kegiatan</th>
                                            <th class="border-b-2 whitespace-no-wrap text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($pendaftaran->pengalamans) > 0)
                                            @foreach($pendaftaran->pengalamans as $pengalaman)
                                                <tr>
                                                    <td class="border-b whitespace-no-wrap">{{ $loop->iteration }}</td>
                                                    <td class="border-b whitespace-no-wrap">{{ $pengalaman->type }}</td>
                                                    <td class="border-b whitespace-no-wrap">{{ $pengalaman->jabatan }}</td>
                                                    <td class="border-b whitespace-no-wrap">{{ $pengalaman->kegiatan }}</td>
                                                    <td class="border-b whitespace-no-wrap flex justify-end items-center">
                                                        @if(auth()->user()->role != 'admin')
                                                            <a type="button" class="flex items-center button button--sm text-white bg-theme-12 ml-1 p-2 w-25" href="{{url('/pendaftaran/'.$pendaftaran->id.'/pengalaman/'.$pengalaman->id.'/edit')}}"> <i data-feather="edit" class="w-4 h-4 mr-1"></i> Edit </a>
                                                            <button class="flex items-center button button--sm text-white bg-theme-6 p-2 m-1 delete-data" type="button" data-redirect="pengalaman" data-id="{{ $pengalaman->id }}" data-token="{{ csrf_token() }}" data-url_special="{{'/pendaftaran/'.$pendaftaran->id.'/pengalaman/'.$pengalaman->id}}"> <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete </button>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td class="border-b whitespace-no-wrap text-center" colspan="5">Tidak terdapat data pengalaman</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- End Pengalaman --}}
                </div>
                <div class="flex justify-end gap-2">
                    @if(auth()->user()->role == 'admin')
                        <div class="flex justify-end mt-4">
                            <a href="{{url('/pendaftaran')}}" type="submit" class="button w-20  bg-gray-600 text-white ml-auto">Kembali</a>
                        </div>
                        @if($pendaftaran->status != 'verified' && $pendaftaran->status !='cancel')
                            <div class="flex justify-end mt-4">
                                <button type="button" class="button w-20  bg-green-500 text-white ml-auto btn-change-status" data-status="verified" data-id="{{$pendaftaran->id}}" data-token="{{ csrf_token() }}">Verifikasi</button>
                            </div>
                        @endif
                        @if($pendaftaran->status == 'submit' || $pendaftaran->status =='resubmit')
                            <div class="flex justify-end mt-4">
                                <button type="button" class="button w-30  bg-theme-12 text-white ml-auto btn-change-status" data-status="unverified" data-id="{{$pendaftaran->id}}" data-token="{{ csrf_token() }}">Tolak Verifikasi</button>
                            </div>
                        @endif

                        @if($pendaftaran->status != 'verified' && $pendaftaran->status !='cancel')
                            <div class="flex justify-end mt-4">
                                <button type="button" class="button w-20  bg-theme-6 text-white ml-auto btn-change-status" data-status="cancel" data-id="{{$pendaftaran->id}}" data-token="{{ csrf_token() }}">Cancel</button>
                            </div>
                        @endif
                    @endif
                    @if(auth()->user()->role != 'admin' && ($pendaftaran->status == 'pending' || $pendaftaran->status == 'unverified'))
                        <div class="flex justify-end mt-4">
                            <button type="submit" name="ajukan{{$pendaftaran->status != 'pending' ? '_ulang' : ''}}" value="1" class="button {{$pendaftaran->status == 'pending' ? 'w-20' : 'w-30'}} bg-theme-12 text-white ml-auto">Daftar {{$pendaftaran->status != 'pending' ? 'Ulang' : ''}}</button>
                        </div>
                        <div class="flex justify-end mt-4">
                            <button type="submit" class="button w-30 bg-theme-1 text-white ml-auto">Simpan Sementara</button>
                        </div>
                    @endif
                </div>
               
            </form>
        </div>
    </div>
@endsection

@section('js-extra')
@if($errors->any())
    <script>
        $(document).ready(function() {
            Swal.fire(
                'Perhatian!',
                'Harap Periksa Kelengkapan Form Anda',
                'info'
            );
        });
    </script>
@endif
<script>
    $('.input-toggle').on('change', function () {
        var value = $(this).val();
        var value_change = value == 0 ? 1 : 0;
        var type = $(this).data('type');

        if (value_change == 1) {
            $.each($('.input-' + type), function (key, val) {
                $(val).attr('readonly', true);
                $(val).addClass('bg-gray-200');
            });
        } else {
            $.each($('.input-' + type), function (key, val) {
                $(val).removeAttr('readonly');
                $(val).removeClass('bg-gray-200');
            });
        }
        $(this).val(value_change);
        $('#value_'+type).val(value_change);
    });

    $('.btn-change-status').click(function (e) { 
        e.preventDefault();
        var id = $(this).data("id");
        var status = $(this).data("status");
        var token = $(this).data("token");
        
        if (status == 'unverified') {
            Swal.fire({
                title: 'Catatan untuk pendaftar',
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Kirim',
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                if (result.isConfirmed) {
                    changeStatus(id,status,token,result.value);
                }
            })
        } else {
            Swal.fire({
                title: 'Yakin ingin mengubah?',
                text: "Mohon periksa kembali",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0275d8',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ubah',
                cancelButtonText: 'Batal'
            })
            .then((result) => {
                if (result.isConfirmed) {
                    changeStatus(id,status,token,'');
                }
            })
        }
        
    });

    function changeStatus(id, status, token, message) {
        $.ajax({
            url: '/pendaftaran/'+id+'/changestatus',
            data: {
                "id": id,
                "status": status,
                "message": message,
                "_method": 'GET',
                "_token": token,
            },
            success: function (response) {
                
                Swal.fire(
                    'Berhasil!',
                    response.message,
                    'success'
                )
                .then((result) => {
                    location.reload();
                });

            }
        });
    }
</script>
@endsection