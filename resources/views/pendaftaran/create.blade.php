@extends('layouts.app')
@section('content')
    <div class="content">
        <div class="grid grid-cols-12 gap-6">
            <!-- BEGIN: Profile Menu -->
            <div class="col-span-12 lg:col-span-4 xxl:col-span-3 flex lg:block flex-col-reverse">
                <div class="intro-y box mt-5">

                    <div class="p-5 border-t border-gray-200" >
                        <li class="flex items-center cursor-pointer hover:text-blue-500 text-blue-600 tab-menu-pendaftaran" id="tab-pendaftaran-personal" href="" onclick="openTab('personal')"> <i data-feather="user" class="w-4 h-4 mr-2"></i> Data Pribadi </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-perkuliahan" onclick="openTab('perkuliahan')" > <i data-feather="layers" class="w-4 h-4 mr-2"></i> Perkuliahan </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-pekerjaan" onclick="openTab('pekerjaan')" > <i data-feather="sliders" class="w-4 h-4 mr-2"></i> Pekerjaan </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-riwayat" onclick="openTab('riwayat')" > <i data-feather="archive" class="w-4 h-4 mr-2"></i> Riwayat </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-prestasi" onclick="openTab('prestasi')" > <i data-feather="star" class="w-4 h-4 mr-2"></i> Prestasi </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-pengalaman" onclick="openTab('pengalaman')" > <i data-feather="github" class="w-4 h-4 mr-2"></i> Pengalaman </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-suratpendukung" onclick="openTab('suratpendukung')" > <i data-feather="inbox" class="w-4 h-4 mr-2"></i> Surat Pendukung </li>
                        <li class="flex items-center cursor-pointer mt-5  hover:text-blue-500 tab-menu-pendaftaran" href="" id="tab-pendaftaran-lainnya" onclick="openTab('lainnya')" > <i data-feather="more-vertical" class="w-4 h-4 mr-2"></i> Lainnya </li>
                    </div>
                </div>
            </div>
            <!-- END: Profile Menu -->
            <form action="/pendaftaran" method="post" enctype="multipart/form-data" class="col-span-12 lg:col-span-8 xxl:col-span-9">
                @csrf
                <div class="col-span-12 lg:col-span-8 xxl:col-span-9" >
                    <!-- BEGIN: Personal Information -->
                    <div class="intro-y box lg:mt-5 tab-content" id="personal" style="display: block"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Data Diri
                            </h2>
                        </div>
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 xl:col-span-6">
                                    <div>
                                        <label>Nama</label>
                                        <input type="text" name="" class="input w-full border mt-2" placeholder="" value="{{auth()->user()->name}}" >
                                    </div>
                                    <div class="mt-3">
                                        <label>Nama Lengkap</label>
                                        <input type="text" name="nama_lengkap"  class="input w-full border mt-2" placeholder="Masukkan Nama Lengkap" value="" >
                                        @if($errors->any() && $errors->get('nama_lengkap') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Tempat Lahir</label>
                                        <input type="text" name="tempat_lahir" class="input w-full border mt-2" placeholder="Masukkan Tempat Lahir" value="" >
                                        @if($errors->any() && $errors->get('tempat_lahir') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Kota</label>
                                        <input type="text" name="kota" class="input w-full border mt-2" placeholder="Masukkan Nama Kota" value="">
                                        @if($errors->any() && $errors->get('kota') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>No Hp</label>
                                        <input type="text" name="no_hp" class="input w-full border mt-2" placeholder="Masukkan No Handphone" value="">
                                        @if($errors->any() && $errors->get('no_hp') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-4">
                                        <label>Ormawa</label>
                                        <select name="ormawa_id" class="input w-full border mt-3">
                                            @foreach ($ormawas as $ormawa)
                                                <option value="{{$ormawa->id}}">{{$ormawa->ormawa}}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->any() && $errors->get('ormawa_id') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Orang Tua</label>
                                        <select name="orang_tua" class="input w-full border mt-3">
                                            <option value="Ayah" >Ayah</option>
                                            <option value="Ibu" >Ibu</option>
                                            <option value="Wali" >Wali</option>
                                        </select>
                                        @if($errors->any() && $errors->get('orang_tua') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Alamat Orang Tua</label>
                                        <input type="text" name="alamat_orang_tua" class="input w-full border mt-2" placeholder="Masukkan Alamat Orang Tua" value="">
                                        @if($errors->any() && $errors->get('alamat_orang_tua') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3" >
                                        <div class="flex items-center">
                                            <label>Tanda Tangan Orang Tua</label>
                                        </div>
                                        <input type="file" name="tanda_tangan_orangtua" class="input w-full border mt-2" placeholder="Input text" value="">
                                        @if($errors->any() && $errors->get('tanda_tangan_orangtua') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-6">
                                    <div>
                                        <label>Email</label>
                                        <input type="text" name="" class="input w-full border mt-2" placeholder="" value="{{auth()->user()->email}}">
                                    </div>
                                    <div class="mt-3">
                                        <label>Nama Panggilan</label>
                                        <input type="text" name="nama_panggilan" class="input w-full border mt-2" placeholder="Masukkan Nama Panggilan" value="">
                                        @if($errors->any() && $errors->get('nama_panggilan') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Tanggal Lahir</label>
                                        <input type="date" name="tanggal_lahir" name="nama_panggilan" class="input w-full border mt-2" placeholder="Input text" value="">
                                        @if($errors->any() && $errors->get('tanggal_lahir') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    
                                    <div class="mt-2">
                                        <label>Agama</label>
                                        <select name="agama" class="input w-full border mt-3">
                                            <option value="" >-Pilih Agama-</option>
                                            <option value="Hindu" >Hindu</option>
                                            <option value="Islam" >Islam</option>
                                            <option value="Kristen Katolik" >Kristen Katolik</option>
                                            <option value="Kristen Protestan" >Kristen Protestan</option>
                                            <option value="Budha" >Budha</option>
                                            <option value="Konghucu" >Konghucu</option>
                                            <option value="Lainnya" >Lainnya</option>
                                        </select>
                                        @if($errors->any() && $errors->get('agama') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Hobi</label>
                                        <input type="text" name="hobi" class="input w-full border mt-2" placeholder="Masukkan Nama Hobi" value="">
                                        @if($errors->any() && $errors->get('hobi') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-4">
                                        <label>Nama Orang Tua</label>
                                        <input type="text" name="nama_orang_tua" class="input w-full border mt-2" placeholder="Masukkan Nama Orang Tua" value="">
                                        @if($errors->any() && $errors->get('nama_orang_tua') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-4">
                                        <label>No Telp Orang Tua</label>
                                        <input type="text" name="no_telp_orang_tua" class="input w-full border mt-2" placeholder="Masukkan No Telp Orang Tua" value="">
                                        @if($errors->any() && $errors->get('no_telp_orang_tua') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-2 flex items-center">
                                        <label>Foto</label>
                                        <div class="menu__icon ml-2"> <i data-feather="info"></i> </div>
                                    </div>
                                    <input type="file" name="foto" class="input w-full border mt-2" placeholder="Input text" value="">
                                    @if($errors->any() && $errors->get('foto') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                </div>
                                <div class="col-span-12" >
                                    <div class="">
                                        <label>Alamat</label>
                                        <textarea type="text" name="alamat" name="nama_panggilan" class="input w-full border mt-2" placeholder="Masukkan Alamat" value=""></textarea>
                                        @if($errors->any() && $errors->get('nama_panggilan') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <!-- END: Personal Information -->

                    {{-- Perkuliahan --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="perkuliahan" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Perkuliahan
                            </h2>
                        </div>
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 xl:col-span-6">
                                    <div>
                                        <label>Nim</label>
                                        <input type="text" name="nim" class="input w-full border mt-2" placeholder="Masukkan Nim" value="" >
                                        @if($errors->any() && $errors->get('nim') != null)
                                        <div class="mt-1" >
                                            <i class="text-red-600 text-sm">
                                                {{ $errors->get('nim')[0]}}
                                            </i>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-6">
                                    <label>Prodi</label>
                                    <select name="prodi" class="input w-full border mt-2">
                                        <option value="Sistem Informasi" >Sistem Informasi</option>
                                        <option value="Sistem Komputer" >Sistem Komputer</option>
                                        <option value="Teknologi Informasi" >Teknologi Informasi</option>
                                    </select>
                                    @if($errors->any() && $errors->get('prodi') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                </div>
                                <div class="col-span-12 ">
                                    <div>
                                        <label>Semester</label>
                                        <input type="text" name="semester" class="input w-full border mt-2" placeholder="Masukkan Nama Semester" value="">
                                        @if($errors->any() && $errors->get('semester') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-6" >
                                    <div>
                                        <div class="flex items-center">
                                            <label>KRS Terakhir</label>
                                        </div>
                                        <input type="file" name="krs_terakhir" class="input w-full border mt-2" placeholder="Input text" value="">
                                        @if($errors->any() && $errors->get('krs_terakhir') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>  
                                    <div class="mt-3" >
                                        <div class="flex items-center">
                                            <label>Foto KTM</label>
                                        </div>
                                        <input type="file" name="foto_ktm" class="input w-full border mt-2" placeholder="Input text" value="">
                                        @if($errors->any() && $errors->get('foto_ktm') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    
                                </div>
                                <div class="col-span-12 xl:col-span-6" >
                                    <div>
                                        <div class="flex items-center">
                                            <label>KHS Terakhir</label>
                                        </div>
                                        <input type="file" name="khs_terakhir" class="input w-full border mt-2" placeholder="Input text" value="">
                                        @if($errors->any() && $errors->get('khs_terakhir') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <div class="flex items-center">
                                            <label>Sertifikat Kegiatan atau Kepengurusan Ormawa</label>
                                        </div>
                                        <input type="file" name="sertif_kepengurusan" class="input w-full border mt-2" placeholder="Input text" value="">
                                        @if($errors->any() && $errors->get('sertif_kepengurusan') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    {{-- End perkuliahan --}}

                    {{-- Pekerjaan --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="pekerjaan" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <div>
                                <h2 class="font-medium text-base mr-auto">
                                    Pekerjaan
                                </h2>
                            </div>
                            <div class="w-full sm:w-auto flex items-center sm:ml-auto mt-3 sm:mt-0">
                                <div class="mr-3">Tidak Memiliki Pekerjaan</div>
                                <input data-target="#input" class="show-code input input--switch border" type="checkbox">
                            </div>
                        </div>
                        
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 xl:col-span-6">
                                    <div>
                                        <label>Pekerjaan</label>
                                        <input type="text" name="pekerjaan" class="input w-full border mt-2" placeholder="Masukkan Nama Pekerjaan" value="" >
                                    </div>
                                    <div class="mt-3">
                                        <label>Jabatan Kerja</label>
                                        <input type="text" name="jabatan_pekerjaan" class="input w-full border mt-2" placeholder="Masukkan Jabatan Pekerjaan" value="" >
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-6">
                                    <div>
                                        <label>Lokasi Kerja</label>
                                        <input type="text" name="lokasi_kerja" class="input w-full border mt-2" placeholder="Masukkan Lokasi Pekerjaan" value="">
                                    </div>
                                    <div class="mt-3">
                                        <label>Tahun Kerja</label>
                                        <input type="date" name="tahun_kerja" class="input w-full border mt-2" placeholder="Masukkan Tahun Kerja" value="">
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    {{-- End Pekerjaan --}}

                    {{-- Riwayat --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="riwayat" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Riwayat
                            </h2>
                        </div>
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12">
                                    <div>
                                        <div class="flex">
                                            <label>Sakit Yang Pernah Diderita</label>
                                            <div class="w-full sm:w-auto flex items-center sm:ml-auto mt-3 sm:mt-0">
                                                <div class="mr-3">Tidak Memiliki Sakit Yang Pernah Diderita</div>
                                                <input data-target="#input" class="show-code input input--switch border" type="checkbox">
                                            </div>
                                        </div>
                                        <input type="text" name="sakit_pernah_diderita" class="input w-full border mt-2" placeholder="Masukkan Sakit Yang Pernah Diderita" value="" >
                                    </div>
                                    <div class="mt-3">
                                        <label>Pendidikan Terakhir / Asal Sekolah</label>
                                        <input type="text" name="pendidikan_terakhir" class="input w-full border mt-2" placeholder="Masukkan Pendidikan Terakhir" value="" >
                                        @if($errors->any() && $errors->get('pendidikan_terakhir') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    {{-- End Riwayat --}}

                    {{-- Surat pendukung --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="suratpendukung" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Surat Pendukung
                            </h2>
                        </div>
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 ">
                                    <div>
                                        <label>Surat Pendukung</label>
                                        <input type="file" name="surat_dukungan" class="input w-full border mt-2" placeholder="" value="" >
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    {{-- End Surat pendukung --}}

                    {{-- Lainnya --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="lainnya" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Lainnya
                            </h2>
                        </div>
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 xl:col-span-6">
                                    <div class="">
                                        <label>Type</label>
                                        <select name="type" class="input w-full border mt-2">
                                            <option value="">-Pilih Type-</option>
                                            <option value="Bem">Bem</option>
                                            <option value="Balma">Balma</option>
                                            <option value="Hima">Hima</option>
                                        </select>
                                        @if($errors->any() && $errors->get('type') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-10">
                                        <label>Yang Disuka</label>
                                        <textarea type="text" name="yang_disuka" class="input w-full border mt-2" placeholder="Masukkan Hal Yang Disukai" value="" ></textarea>
                                        @if($errors->any() && $errors->get('yang_disuka') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Kebiasaan Buruk</label>
                                        <textarea type="text" name="kebiasaan_buruk" class="input w-full border mt-2" placeholder="Masukkan Kebiasaan Buruk Yang Sering Dilakukan" value="" ></textarea>
                                        @if($errors->any() && $errors->get('kebiasaan_buruk') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Tujuan</label>
                                        <textarea type="text" name="tujuan" class="input w-full border mt-2" placeholder="Tujuan Mendaftarkan Diri Menjadi Bem/Balma/Hima" value="" ></textarea>
                                        @if($errors->any() && $errors->get('tujuan') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Yang Akan Dilakukan</label>
                                        <textarea type="text" name="yang_akan_dilakukan" class="input w-full border mt-2" placeholder="Yang Akan Dilakukan Jika Menjadi Bem/Balma/Hima" value=""></textarea>
                                        @if($errors->any() && $errors->get('yang_akan_dilakukan') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Motivasi</label>
                                        <textarea type="text" name="motivasi" class="input w-full border mt-2" placeholder="Motivasi Kamu Menjadi Bem/Balma/Hima" value=""></textarea>
                                        @if($errors->any() && $errors->get('motivasi') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-6">
                                    <div class="">
                                        <label>Sebagai</label>
                                        <select name="type_sebagai" class="input w-full border mt-2">
                                            <option value="">-Pilih Sebagai-</option>
                                            <option value="Ketua">Ketua</option>
                                            <option value="Wakil">Wakil</option>
                                        </select>
                                        @if($errors->any() && $errors->get('type_sebagai') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-10">
                                        <label>Yang Tidak Disuka</label>
                                        <textarea type="text" name="yang_tidak_disuka" class="input w-full border mt-2" placeholder="Masukkan Hal Yang Tidak Disukai" value=""></textarea>
                                        @if($errors->any() && $errors->get('yang_tidak_disuka') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Motto</label>
                                        <textarea type="text" name="motto" class="input w-full border mt-2" placeholder="Masukkan Motto Kamu" value=""></textarea>
                                        @if($errors->any() && $errors->get('motto') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Alasan</label>
                                        <textarea type="text" name="alasan" class="input w-full border mt-2" placeholder="Alasan Kamu Ingin Menjadi Bem/Balma/Hima" value=""></textarea>
                                        @if($errors->any() && $errors->get('alasan') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Penjelasan Diri</label>
                                        <textarea type="text" name="penjelasan_diri" class="input w-full border mt-2" placeholder="Deskripsikan Diri Kamu Ingin Menjadi Bem/Balma/Hima" value=""></textarea>
                                        @if($errors->any() && $errors->get('penjelasan_diri') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3">
                                        <label>Visi Misi</label>
                                        <textarea type="text" name="visi_misi" class="input w-full border mt-2" placeholder="Masukkan Visi Misi Kamu" value=""></textarea>
                                        @if($errors->any() && $errors->get('visi_misi') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="mt-3" >
                                        <div class="flex items-center">
                                            <label>Tanda Tangan</label>
                                        </div>
                                        <input type="file" name="tanda_tangan" class="input w-full border mt-2" placeholder="Input text" value="">
                                        @if($errors->any() && $errors->get('tanda_tangan') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    {{-- End Lainnya --}}

                    {{-- Prestasi --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="prestasi" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Prestasi
                            </h2>
                            <input type="hidden" id="count-prestasi" value="1">
                            <a href="{{ url('/pendaftaran/') }}" type="button" class="button w-20 bg-theme-12 text-white ml-auto">Tambah</a>
                        </div>
                        <div id="div-prestasi">
                            <div class="p-5">
                                <div class="grid grid-cols-12 gap-5">
                                    <div class="col-span-12 xl:col-span-6">
                                        <div class="">
                                            <label>Type</label>
                                            <select name="type_prestasi" class="input w-full border mt-2">
                                                <option value="">-Pilih Type-</option>
                                                <option value="Akademik">Akademik</option>
                                                <option value="Non Akademik">Non Akademik</option>
                                            </select>
                                            @if($errors->any() && $errors->get('type_prestasi') != null)
                                                <div class="mt-1" >
                                                    <i class="text-red-600 text-sm">
                                                        *Field Wajib Dilengkapi
                                                    </i>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-span-12 xl:col-span-6">
                                        <div class="">
                                            <label>Prestasi</label>
                                            <textarea type="text" name="deskripsi_prestasi" class="input w-full border mt-2" placeholder="Deskripsi Prestasi" value="" ></textarea>
                                            @if($errors->any() && $errors->get('deskripsi_prestasi') != null)
                                                <div class="mt-1" >
                                                    <i class="text-red-600 text-sm">
                                                        *Field Wajib Dilengkapi
                                                    </i>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- End Prestasi --}}

                    {{-- Pengalaman --}}
                    <div class="intro-y box lg:mt-5 tab-content" id="pengalaman" style="display: none"  >
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Pengalaman
                            </h2>
                            <button type="" class="button w-20 bg-theme-12 text-white ml-auto">Tambah</button>
                        </div>
                        <div class="p-5">
                            <div class="grid grid-cols-12 gap-5">
                                <div class="col-span-12 xl:col-span-6">
                                    <div class="">
                                        <label>Type</label>
                                        <select name="type_pengalaman" class="input w-full border mt-2">
                                            <option value="">-Pilih Type-</option>
                                            <option value="Organisasi">Organisasi</option>
                                            <option value="Kepanitiaan">Kepanitiaan</option>
                                        </select>
                                        @if($errors->any() && $errors->get('type_pengalaman') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-6">
                                    <div class="">
                                        <label>Jabatan</label>
                                        <input type="text" name="jabatan_pengalaman" class="input w-full border mt-2" placeholder="Jabatan" value="" />
                                        @if($errors->any() && $errors->get('jabatan_pengalaman') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-span-12 xl:col-span-12">
                                    <div class="">
                                        <label>Prestasi</label>
                                        <textarea type="text" name="deskripsi_pengalaman" class="input w-full border mt-2" placeholder="Jabatan" value="" ></textarea>
                                        @if($errors->any() && $errors->get('deskripsi_pengalaman') != null)
                                            <div class="mt-1" >
                                                <i class="text-red-600 text-sm">
                                                    *Field Wajib Dilengkapi
                                                </i>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- End Prestasi --}}
                </div>
                <div class="flex justify-end gap-2">
                    <div class="flex justify-end mt-4">
                        <a href="{{url('/pendaftaran')}}" type="submit" class="button w-20  bg-gray-600 text-white ml-auto">Kembali</a>
                    </div>
                    <div class="flex justify-end mt-4">
                        <button type="submit" class="button w-20 bg-theme-1 text-white ml-auto">Simpan</button>
                    </div>
                </div>
               
            </form>
        </div>
    </div>
@endsection