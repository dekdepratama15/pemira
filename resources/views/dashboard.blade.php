@extends('layouts.app')

@section('content')
<div class="content">
    <div class="grid grid-cols-12 gap-6">
    <div class="col-span-12 xxl:col-span-9 grid grid-cols-12 gap-6">
                    <!-- BEGIN: General Report -->
                    <div class="col-span-12 mt-8">
                        <div class="intro-y flex items-center h-10">
                            <h2 class="text-lg font-medium truncate mr-5">
                                General 
                            </h2>
                        </div>
                        <div class="grid grid-cols-12 gap-6 mt-5">
                            <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                                <div class="report-box zoom-in">
                                    <div class="box p-5">
                                        <div class="flex">
                                            <i data-feather="clipboard" class="report-box__icon text-theme-10"></i> 
                                            <div class="ml-auto">
                                            </div>
                                        </div>
                                        <div class="text-3xl font-bold leading-8 mt-6">{{count($data['pendaftaran'])}}</div>
                                        <div class="text-base text-gray-600 mt-1">Total Pendaftaran</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                                <div class="report-box zoom-in">
                                    <div class="box p-5">
                                        <div class="flex">
                                            <i data-feather="info" class="report-box__icon text-theme-11"></i> 
                                            <div class="ml-auto">
                                            </div>
                                        </div>
                                        <div class="text-3xl font-bold leading-8 mt-6">{{count($data['informasi'])}}</div>
                                        <div class="text-base text-gray-600 mt-1">Total Informasi</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                                <div class="report-box zoom-in">
                                    <div class="box p-5">
                                        <div class="flex">
                                            <i data-feather="slack" class="report-box__icon text-theme-12"></i> 
                                            <div class="ml-auto">
                                            </div>
                                        </div>
                                        <div class="text-3xl font-bold leading-8 mt-6">{{count($data['ormawa'])}}</div>
                                        <div class="text-base text-gray-600 mt-1">Total Ormawa</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                                <div class="report-box zoom-in">
                                    <div class="box p-5">
                                        <div class="flex">
                                            <i data-feather="users" class="report-box__icon text-theme-9"></i> 
                                            <div class="ml-auto">
                                            </div>
                                        </div>
                                        <div class="text-3xl font-bold leading-8 mt-6">{{count($data['user'])}}</div>
                                        <div class="text-base text-gray-600 mt-1">Total User</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: General Report -->
                    <!-- BEGIN: Weekly Top Seller -->
                    <div class="col-span-12 sm:col-span-6 lg:col-span-3 mt-8">
                        <div class="intro-y flex items-center h-10">
                            <h2 class="text-lg font-medium truncate mr-5">
                                Status Pendaftaran
                            </h2>
                            <a href="{{url('/pendaftaran')}}" class="ml-auto text-theme-1 truncate">See all</a> 
                        </div>
                        <div class="intro-y box p-5 mt-5">
                            <canvas class="mt-3" id="report-pie-chart" height="280"></canvas>
                            <div class="mt-8">
                                @foreach($status as $st)
                                <div class="flex items-center">
                                    <div class="w-2 h-2 bg-{{$st['color_name']}}-500 rounded-full mr-3"></div>
                                    <span class="truncate">{{$st['status']}}</span> 
                                    <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                                    <span class="font-medium xl:ml-auto">{{$st['persentase']}}%</span> 
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- END: Weekly Top Seller -->
                    <!-- BEGIN: Sales Report -->
                    <div class="col-span-12 sm:col-span-6 lg:col-span-3 mt-8">
                        <div class="intro-y flex items-center h-10">
                            <h2 class="text-lg font-medium truncate mr-5">
                                Tipe Pendaftar
                            </h2>
                            <a href="{{url('/pendaftaran')}}" class="ml-auto text-theme-1 truncate">See all</a> 
                        </div>
                        <div class="intro-y box p-5 mt-5">
                            <canvas class="mt-3" id="report-donut-chart" height="280"></canvas>
                            <div class="mt-8">
                                <div class="flex items-center">
                                    <div class="w-2 h-2 bg-theme-11 rounded-full mr-3"></div>
                                    <span class="truncate">Bem</span> 
                                    <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                                    <span class="font-medium xl:ml-auto">{{$type[0]}}</span> 
                                </div>
                                <div class="flex items-center mt-4">
                                    <div class="w-2 h-2 bg-theme-1 rounded-full mr-3"></div>
                                    <span class="truncate">Balma</span> 
                                    <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                                    <span class="font-medium xl:ml-auto">{{$type[1]}}</span> 
                                </div>
                                <div class="flex items-center mt-4">
                                    <div class="w-2 h-2 bg-theme-12 rounded-full mr-3"></div>
                                    <span class="truncate">Hima</span> 
                                    <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                                    <span class="font-medium xl:ml-auto">{{$type[2]}}</span> 
                                </div>
                                <div class="flex items-center mt-4">
                                    <div class="w-2 h-2 bg-gray-500 rounded-full mr-3"></div>
                                    <span class="truncate">Belum memilih</span> 
                                    <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                                    <span class="font-medium xl:ml-auto">{{$type[3]}}</span> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Sales Report -->
                    <!-- BEGIN: Weekly Best Sellers -->
                    <div class="col-span-12 xl:col-span-6 mt-8">
                        <div class="intro-y flex items-center h-10">
                            <h2 class="text-lg font-medium truncate mr-5">
                                Pendaftar Terbaru
                            </h2>
                        </div>
                        <div class="mt-5">
                            @foreach($user_new as $user)
                            <div class="intro-y">
                                <div class="box px-4 py-4 mb-3 flex items-center zoom-in">
                                    <div class="w-10 h-10 flex-none image-fit rounded-md overflow-hidden">
                                        <img alt="Midone Tailwind HTML Admin Template" src="{{!empty($user->img_profile) ? url('/upload/'.$user->img_profile) : url('dist/images/user-default.png') }}">
                                    </div>
                                    <div class="ml-4 mr-auto">
                                        <div class="font-medium">{{$user->name}}</div>
                                        <div class="text-gray-600 text-xs">{{date('d M Y ', strtotime($user->created_at))}}</div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <a href="{{url('/pengguna')}}" class="intro-y w-full block text-center rounded-md py-4 border border-dotted border-theme-15 text-theme-16">View All</a> 
                        </div>
                    </div>
                    <!-- END: Weekly Best Sellers -->
                </div>
    </div>
</div>
@endsection