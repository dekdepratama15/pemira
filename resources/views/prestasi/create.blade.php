@extends('layouts.app')
@section('content')
    <div class="content">
        <div class="grid grid-cols-12 gap-6">
            <!-- BEGIN: Profile Menu -->
            <!-- END: Profile Menu -->
            <form action="/pendaftaran/{{$pendaftaran_id}}/prestasi" method="post" enctype="multipart/form-data" class="col-span-12 lg:col-span-12 xxl:col-span-12">
                @csrf
                <div class="col-span-12 lg:col-span-12 xxl:col-span-12" >
                    <div class="intro-y box lg:mt-5 tab-content" id="prestasi">
                        <div class="flex items-center p-5 border-b border-gray-200">
                            <h2 class="font-medium text-base mr-auto">
                                Tambah Prestasi
                            </h2>
                        </div>
                        <div id="div-prestasi">
                            <div class="p-5">
                                <div class="grid grid-cols-12 gap-5">
                                    <div class="col-span-12 xl:col-span-6">
                                        <div class="">
                                            <label>Type</label>
                                            <select name="type_prestasi" class="input w-full border mt-2" required>
                                                <option value="">-Pilih Type-</option>
                                                <option value="Akademik">Akademik</option>
                                                <option value="Non Akademik">Non Akademik</option>
                                            </select>
                                            @if($errors->any() && $errors->get('type_prestasi') != null)
                                                <div class="mt-1" >
                                                    <i class="text-red-600 text-sm">
                                                        *Field Wajib Dilengkapi
                                                    </i>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-span-12 xl:col-span-6">
                                        <div class="">
                                            <label>Prestasi</label>
                                            <textarea type="text" name="deskripsi_prestasi" class="input w-full border mt-2" placeholder="Deskripsi Prestasi" value="" required></textarea>
                                            @if($errors->any() && $errors->get('deskripsi_prestasi') != null)
                                                <div class="mt-1" >
                                                    <i class="text-red-600 text-sm">
                                                        *Field Wajib Dilengkapi
                                                    </i>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="flex justify-end gap-2">
                    <div class="flex justify-end mt-4">
                        <a href="{{url('/pendaftaran/'.$pendaftaran_id.'/edit')}}" type="submit" class="button w-20  bg-gray-600 text-white ml-auto">Kembali</a>
                    </div>
                    <div class="flex justify-end mt-4">
                        <button type="submit" class="button w-20 bg-theme-1 text-white ml-auto">Simpan</button>
                    </div>
                </div>
               
            </form>
        </div>
    </div>
@endsection