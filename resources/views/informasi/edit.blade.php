@extends('layouts.app')

@section('content')
<div class="content">
    <h2 class="intro-y text-lg font-medium mt-10">
        Edit Informasi
    </h2>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
            <div class="hidden md:block mx-auto text-gray-600"></div>
            <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            </div>
        </div>
        <!-- BEGIN: Data List -->
        <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <form action="{{route('informasi.update',$informasi->id)}}" method="post" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="flex flex-col" >
                <div class="col-span-12 flex">
                    <div class="col-span-4 w-full">
                        <label for="ormawa" class="block text-sm font-medium leading-6 text-gray-900">Type</label>
                        <div class="mt-2">
                            <select name="type" id="" class="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 h-10">
                                <option value="">-Pilih Type-</option>
                                <option value="Semua" {{$informasi->type == 'Semua' ? 'selected' : ''}}>Semua</option>
                                <option value="Bem" {{$informasi->type == 'Bem' ? 'selected' : ''}}>Bem</option>
                                <option value="Balma" {{$informasi->type == 'Balma' ? 'selected' : ''}}>Balma</option>
                                <option value="Hima" {{$informasi->type == 'Hima' ? 'selected' : ''}}>Hima</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-span-4 w-full mx-2">
                        <label for="judul" class="block text-sm font-medium leading-6 text-gray-900">Judul</label>
                        <div class="mt-2">
                            <input id="judul" value="{{$informasi->judul}}" name="judul" type="text" placeholder="Masukkan Judul" autocomplete="judul" class="block w-full rounded-md border-0 py-1.5 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 h-10">
                        </div>
                    </div>
                    <div class="col-span-4 w-full">
                        <label for="file" class="block text-sm font-medium leading-6 text-gray-900">File</label>
                        <div class="mt-2 flex">
                            <input id="file" name="file" type="file" placeholder="Masukkan Nama Ormawa" autocomplete="ormawa" class="block w-full bg-white rounded-md border-0 py-1 px-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 h-10">
                            <a class="flex items-center button button--sm text-white bg-theme-1 h-10 " href="{{ url('upload/'.$informasi->file) }}"> Lihat </a>
                        </div>
                        <i>Isikan jika ingin mengupload ulang file</i>
                    </div>
                </div>
                <div class="col-span-12 flex mt-2">
                    <div class="col-span-6 w-full">
                        <label for="deskripsi" class="block text-sm font-medium leading-6 text-gray-900">Deskripsi</label>
                        <div class="mt-2">
                            <textarea id="deskripsi" cols="30" rows="5" name="deskripsi" type="text" placeholder="Masukkan Deskripsi" autocomplete="deskripsi" class="block w-full rounded-md border-0 py-2 px-4 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">{{$informasi->deskripsi}}</textarea>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="col-span-6">
            <div class="flex justify-start">
                <a href="{{url('/informasi')}}" class="button text-white bg-gray-600 shadow-md mr-2">Kembali</a>
                <button type="submit" class="button text-white bg-theme-1 shadow-md mr-2">Simpan</button>
            </div>
        </div>
        </form>
</div>
@endsection