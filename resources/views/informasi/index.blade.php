@extends('layouts.app')

@section('content')
<div class="content">
    <h2 class="intro-y text-lg font-medium mt-10">
        Data List Informasi
    </h2>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-2">
            @if(auth()->user()->role == 'admin')
                <a href="{{url('/informasi/create')}}" class="button text-white bg-theme-1 shadow-md mr-2">Tambah Data</a>
            @endif
            <div class="hidden md:block mx-auto text-gray-600"></div>
            <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
                <div class="w-56 relative text-gray-700">
                    <form action="" method="get">
                        <input type="text" name="search" value="{{$search}}" class="input w-56 box pr-10 placeholder-theme-13" placeholder="Search...">
                        <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i> 
                    </form>
                </div>
            </div>
        </div>
        <!-- BEGIN: Data List -->
        <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
            <table class="table table-report -mt-2">
                <thead>
                    <tr>
                        <th class="whitespace-no-wrap">INFORMASI</th>
                        <th class="text-center whitespace-no-wrap">TYPE</th>
                        <th class="text-center whitespace-no-wrap">ACTIONS</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($informasis) > 0)
                        @foreach($informasis as $informasi)
                            <tr class="intro-x">
                                <td>
                                    <a href="{{ route('informasi.show',$informasi->id) }}" class="font-medium whitespace-no-wrap">{{$informasi->judul}}</a> 
                                    <div class="text-gray-600 text-xs whitespace-no-wrap">{{$informasi->deskripsi}}</div>
                                </td>
                                <td class="table-report__action w-56 text-center">
                                    <a href="" class="font-medium whitespace-no-wrap ">{{$informasi->type}}</a> 
                                </td>
                                <td class="table-report__action w-56">
                                    <div class="flex justify-center items-center">
                                        <a class="flex items-center button button--sm text-white bg-theme-1 p-2 m-1" href="{{ route('informasi.show',$informasi->id) }}"> <i data-feather="eye" class="w-4 h-4 mr-1"></i> View </a>
                                        @if(auth()->user()->role == 'admin')
                                            <a class="flex items-center button button--sm text-white bg-theme-12 p-2" href="{{ route('informasi.edit',$informasi->id) }}"> <i data-feather="edit" class="w-4 h-4 mr-1"></i> Edit </a>
                                            <button class="flex items-center button button--sm text-white bg-theme-6 p-2 m-1 delete-data" type="button" data-redirect="informasi" data-id="{{ $informasi->id }}" data-token="{{ csrf_token() }}"> <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete </button>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="border-b whitespace-no-wrap text-center" colspan="3">Tidak terdapat data informasi</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <!-- END: Data List -->
        <!-- BEGIN: Pagination -->
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-no-wrap items-center">
            {{ $informasis->links('vendor.pagination.default') }}    
        </div>
        <!-- END: Pagination -->
    </div>
</div>
@endsection