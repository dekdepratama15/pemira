<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Pendaftaran</title>
    <style>
        .text-center{
            text-align: center;
        }
        .mt-50{
            margin-top: 50px;
        }
        .mt-30{
            margin-top: 30px;
        }
        .p-relative{
            position: relative;
        }
        .border-foto{
            width: 3cm;
            height: 4cm;
            position:absolute;
            top:10%;
            left:50%;
            transform:translate(-50%,-50%);
            border: 1px #000 solid;
        }
        .border-bottom-only{
            border-bottom: 1px #000 solid !important;
        }
        .page-break{
            page-break-after: always;
        }
        .tab-4{
            text-indent: 4em;
        }
        .container{
            /* background-size: cover;
            bacground-repeat: no-repeat;
            bacground-position: center;
            background-image: url({{public_path('dist/images/logo-'.$type.'.png') }}); */
        }
        .watermark{
            position: fixed;
            top: 45%;
            left: 45%;
            width: 100%;
            text-align: center;
            opacity: .6;
            transform: translate(-50%,-50%);
            z-index: -1000;
        }
    </style>
</head>
<body>
    <div class="watermark">
        <img src="{{ public_path('dist/images/logo-'.$type.'.png') }}" alt="">
    </div>   
    <div class="container" style="">
        <!-- Halaman 1 -->
        <h4 class="text-center">FORMULIR IDENTITAS CALON {{strtoupper($pendaftaran->type_sebagai)}} {{$type == 'balma' ? 'BADAN LEGISLATIF MAHASISWA' : ($type == 'bem' ? 'BADAN EKSEKUTIF MAHASISWA':'HIMPUNAN MAHASISWA')}}</h4>
        <h4 class="text-center mt-50">BIODATA DIRI</h4>
        <table class="mt-30 page-break" width="100%" cellpadding="10" cellspacing="0">
            <tr>
                <td width="20%">Nama Lengkap</td>
                <td width="1%">:</td>
                <td class="border-bottom-only">{{$pendaftaran->nama_lengkap}}</td>
                <td rowspan="14" width="25%" class="p-relative">
                    <div class="border-foto">
                        <img src="{{ public_path('upload/'.$pendaftaran->foto) }}" style="width: 3cm;height: 4cm;" alt="">
                        <!-- <p class="text-center"><br><br> Foto <br> 3 x 4 cm</p> -->
                    </div>
                </td>
            </tr>
            <tr>
                <td>Nama Panggilan</td>
                <td>:</td>
                <td class="border-bottom-only">{{$pendaftaran->nama_panggilan}}</td>
            </tr>
            <tr>
                <td>Tempat, Tanggal Lahir</td>
                <td>:</td>
                <td class="border-bottom-only">{{$pendaftaran->tempat_lahir}}, {{date('d M Y',strtotime($pendaftaran->tanggal_lahir))}}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td class="border-bottom-only">{{$pendaftaran->alamat}}</td>
            </tr>
            <tr>
                <td>No. Hp</td>
                <td>:</td>
                <td class="border-bottom-only">{{$pendaftaran->no_hp}}</td>
            </tr>
            <tr>
                <td>Email</td>
                <td>:</td>
                <td class="border-bottom-only">{{$pendaftaran->user->email}}</td>
            </tr>
            <tr>
                <td>Program Studi</td>
                <td>:</td>
                <td class="border-bottom-only">{{$pendaftaran->prodi}}</td>
            </tr>
            <tr>
                <td>Hobi</td>
                <td>:</td>
                <td class="border-bottom-only">{{$pendaftaran->hobi}}</td>
            </tr>
            <tr>
                <td>Riwayat Penyakit</td>
                <td>:</td>
                <td class="border-bottom-only">
                    @if($pendaftaran->memilikiRiwayat == 1)
                        {{$pendaftaran->sakit_pernah_diderita}}
                    @else
                        Tidak memiliki riwayat penyakit
                    @endif
                </td>
            </tr>
            <tr>
                <td>Kebiasaan Buruk</td>
                <td>:</td>
                <td class="border-bottom-only">{{$pendaftaran->kebiasaan_buruk}}</td>
            </tr>
            <tr>
                <td>Yang Disuka</td>
                <td>:</td>
                <td class="border-bottom-only">{{$pendaftaran->yang_disuka}}</td>
            </tr>
            <tr>
                <td>Yang Tidak Disuka</td>
                <td>:</td>
                <td class="border-bottom-only">{{$pendaftaran->yang_tidak_disuka}}</td>
            </tr>
            <tr>
                <td>Motto</td>
                <td>:</td>
                <td class="border-bottom-only">{{$pendaftaran->motto}}</td>
            </tr>
        </table>
        <!-- End Halaman 1 -->

        <!-- Halaman 2 -->
        <h4 class="text-center">DESKRIPSI DIRI</h4>
        <ol type="1" class="mt-30 page-break">
            <li>
                <b>Tujuan Mencalonkan diri menjadi {{ucwords($pendaftaran->type_sebagai)}} {{$type == 'balma' ? 'Badan Legislatif Mahasiswa' : ($type == 'bem' ? 'Badan Eksekutif Mahasiswa':'Himpunan Mahasiswa')}}</b><br>
                <p>{{$pendaftaran->tujuan}}</p><br>
            </li>
            <li>
                <b>Alasan Saya Pantas Menjadi {{ucwords($pendaftaran->type_sebagai)}} {{$type == 'balma' ? 'Badan Legislatif Mahasiswa' : ($type == 'bem' ? 'Badan Eksekutif Mahasiswa':'Himpunan Mahasiswa')}}</b><br>
                <p>{{$pendaftaran->alasan}}</p><br>
            </li>
            <li>
                <b>Hal yang Akan Dilakukan Jika Terpilih</b><br>
                <p>{{$pendaftaran->yang_akan_dilakukan}}</p><br>
            </li>
            <li>
                <b>Penjelasan Diri Secara Singkat</b><br>
                <p>{{$pendaftaran->penjelasan_diri}}</p><br>
            </li>
            <li>
                <b>Motivasi Menjadi {{ucwords($pendaftaran->type_sebagai)}} {{$type == 'balma' ? 'Badan Legislatif Mahasiswa' : ($type == 'bem' ? 'Badan Eksekutif Mahasiswa':'Himpunan Mahasiswa')}}</b><br>
                <p>{{$pendaftaran->motivasi}}</p><br>
            </li>
            <li>
                <b>Visi dan Misi</b><br>
                <p>{{$pendaftaran->visi_misi}}</p><br>
            </li>
        </ol>
        <!-- End Halaman 2 -->

        <!-- Halaman 3 -->
        <h4 class="text-center">DESKRIPSI DIRI</h4>
        <ol type="1" class="mt-30">
            <li>
                <b>Pengalaman Organisasi/Kepanitiaan</b><br>
                <ol type="a">
                    @if(count($pendaftaran->pengalamans) == 0)
                        -
                    @else
                        @foreach($pendaftaran->pengalamans as $pengalaman)
                            <li>{{$pengalaman->jabatan}} - {{$pengalaman->kegiatan}}</li>
                        @endforeach
                    @endif
                </ol><br>
            </li>
            <li>
                <b>Prestasi</b><br>
                <ol type="a">
                    <li>
                        <b>Akademik</b><br>
                        <ol type=".">
                            @if(count($pendaftaran->prestasis) == 0)
                                -
                            @else
                                @foreach($pendaftaran->prestasis as $prestasi)
                                    @if($prestasi->type == 'Akademik')
                                        <li>{{$prestasi->prestasi}}</li>
                                    @endif
                                @endforeach
                            @endif
                        </ol><br>
                    </li>
                    <li>
                        <b>Non Akademik</b><br>
                        <ol type=".">
                            @if(count($pendaftaran->prestasis) == 0)
                                -
                            @else
                                @foreach($pendaftaran->prestasis as $prestasi)
                                    @if($prestasi->type == 'Non Akademik')
                                        <li>{{$prestasi->prestasi}}</li>
                                    @endif
                                @endforeach
                            @endif
                        </ol><br>
                    </li>
                </ol><br>
            </li>
        </ol>
        <p class="mt-50 tab-4">Melalui tanda tangan dibawah ini, saya menyatakan diri telah bersedia mengikuti keseluruhan tahap seleksi dan siap mematuhi segala peraturan yang ada.</p>
        <table style="float: right" cellpadding="10" cellspacing="0">
            <tr>
                <td><p class="text-center">Denpasar, {{date('d - m - Y')}}</p></td>
            </tr>
            <tr>
                <td class="text-center">
                    <img src="{{ public_path('upload/'.$pendaftaran->tanda_tangan) }}" style="width: 100px;height:100px" alt="">
                </td>
            </tr>
            <tr>
                <td><p class="text-center">( {{$pendaftaran->nama_lengkap}} )</p></td>
            </tr>
        </table>
        <!-- End Halaman 3 -->
    </div>
</body>
</html>