<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Surat Izin</title>
    <style>
        .text-center{
            text-align: center;
        }
        .mt-50{
            margin-top: 50px;
        }
        .mt-30{
            margin-top: 30px;
        }
        .p-relative{
            position: relative;
        }
        .border-foto{
            width: 3cm;
            height: 4cm;
            position:absolute;
            top:10%;
            left:50%;
            transform:translate(-50%,-50%);
            border: 1px #000 solid;
        }
        .border-bottom-only{
            border-bottom: 1px #000 solid !important;
        }
        .page-break{
            page-break-after: always;
        }
        .tab-4{
            text-indent: 4em;
        }
        .container{
            /* background-size: cover;
            bacground-repeat: no-repeat;
            bacground-position: center;
            background-image: url({{public_path('dist/images/logo-'.$type.'.png') }}); */
        }
        .watermark{
            position: fixed;
            top: 45%;
            left: 45%;
            width: 100%;
            text-align: center;
            opacity: .6;
            transform: translate(-50%,-50%);
            z-index: -1000;
        }
    </style>
</head>
<body>
    <div class="watermark">
        <img src="{{ public_path('dist/images/logo-'.$type.'.png') }}" alt="">
    </div>   
    <div class="container">
        <!-- Halaman 1 -->
        <h4 class="text-center">SURAT IZIN ORANG TUA / WALI</h4>
        <table class="mt-30" width="100%" cellpadding="10" cellspacing="0">
            <tr>
                <td colspan="3">Saya yang bertandatangan dibawah ini : </td>
            </tr>
            <tr>
                <td width="20%">Nama Lengkap</td>
                <td width="1%">:</td>
                <td class="border-bottom-only" width="79%">{{$pendaftaran->nama_orang_tua}}</td>
            </tr>
            <!-- <tr>
                <td></td>
                <td></td>
                <td>(Ayah / Ibu / Wali)</td>
            </tr> -->
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td class="border-bottom-only">{{$pendaftaran->alamat_orang_tua}}</td>
            </tr>
            <tr>
                <td>No. Hp</td>
                <td>:</td>
                <td class="border-bottom-only">{{$pendaftaran->no_telp_orang_tua}}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4">Adalah orang tua / wali dari : </td>
            </tr>
            <tr>
                <td>Nama</td>
                <td>:</td>
                <td class="border-bottom-only">{{$pendaftaran->nama_lengkap}}</td>
            </tr>
            <tr>
                <td>NIM</td>
                <td>:</td>
                <td class="border-bottom-only">{{$pendaftaran->nim}}</td>
            </tr>
            <tr>
                <td>Program Studi</td>
                <td>:</td>
                <td class="border-bottom-only">{{$pendaftaran->prodi}}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td class="border-bottom-only">{{$pendaftaran->alamat}}</td>
            </tr>
            <tr>
                <td>No. Hp</td>
                <td>:</td>
                <td class="border-bottom-only">{{$pendaftaran->no_hp}}</td>
            </tr>
            <tr>
                <td colspan="3">
                    <p>Dengan ini menyatakan bahwa saya :</p>
                    <ol type="1">
                        <li>
                            Memberikan izin kepada putra/i saya untuk mengikuti seleksi {{ucwords($pendaftaran->type_sebagai)}} {{$type == 'balma' ? 'Badan Legislatif Mahasiswa' : ($type == 'bem' ? 'Badan Eksekutif Mahasiswa':'Himpunan Mahasiswa')}} <br>
                        </li>
                        <li>
                            Tidak akan menuntut {{$type == 'balma' ? 'Badan Legislatif Mahasiswa' : ($type == 'bem' ? 'Badan Eksekutif Mahasiswa':'Himpunan Mahasiswa')}} jika terjadi sesuatu terhadap putra/i saya, yang disebabkan oleh kelalaian putra/i saya sendiri maupun hal – hal lain diluar batas kemampuan {{$type == 'balma' ? 'Badan Legislatif Mahasiswa' : ($type == 'bem' ? 'Badan Eksekutif Mahasiswa':'Himpunan Mahasiswa')}} <br>
                        </li>
                    </ol>
                </td>
            </tr>
        </table>

        <table style="float: right" cellpadding="10" cellspacing="0">
            <tr>
                <td><p class="text-center">Denpasar, {{date('d - m - Y')}}</p></td>
            </tr>
            <tr>
                <td class="text-center">
                    <img src="{{ public_path('upload/'.$pendaftaran->tanda_tangan_orangtua) }}" style="width: 100px;height:100px" alt="">
                </td>
            </tr>
            <tr>
                <td><p class="text-center">( {{$pendaftaran->nama_orang_tua}} )</p></td>
            </tr>
        </table>
        <!-- End Halaman 1 -->

    </div>
</body>
</html>