<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ormawa extends Model
{
    public function pendaftaran()
    {
        return $this->hasMany(Pendaftaran::class);
    }
}
