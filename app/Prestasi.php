<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prestasi extends Model
{
    public function pendaftaran()
    {
        return $this->belongsTo(Pendaftaran::class);
    }
}
