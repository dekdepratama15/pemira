<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendNotifTelegram($message)
    {
        $chat_id = -999256922;
        $message = urlencode($message);
        $url = "https://api.telegram.org/bot6292376545:AAFSFRSLXW-qNNicNEE0IOvd76Z5VZR4j_E/sendMessage?chat_id=$chat_id&text=$message&parse_mode=HTML";

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        return $response;
    }
    
    public function uploadGambar($file)
    {
        $imgName = '';
        $extension  = $file->getClientOriginalExtension();
        if (in_array($extension, ['jpg', 'png', 'jpeg', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx'])) {
            $imgName = rand(0, 999) . now()->timestamp . '.' . $extension;
            $file->move('upload', $imgName);
        }
        return $imgName;
    }

    public function alertForStatus($status)
    {
        if ($status == 'pending') {
            $response = [
                'color'   => 'gray',
                'icon'    => 'info',
                'title'   => 'Perhatian',
                'message' => 'Anda belum melakukan pendaftaran sebagai Calon, lengkapi data dan tekan tombol Daftar untuk mendaftar',
            ];
        } else if ($status == 'submit') {
            $response = [
                'color'   => 'blue',
                'icon'    => 'clock',
                'title'   => 'Telah Mendaftar',
                'message' => 'Pendaftaran anda telah diajukan, harap menunggu verifikasi dari Admin',
            ];
        } else if ($status == 'resubmit') {
            $response = [
                'color'   => 'blue',
                'icon'    => 'clock',
                'title'   => 'Telah Mendaftar Ulang',
                'message' => 'Pendaftaran anda telah diajukan, harap menunggu verifikasi dari Admin',
            ];
        } else if ($status == 'unverified') {
            $response = [
                'color'   => 'yellow',
                'icon'    => 'alert-triangle',
                'title'   => 'Tidak Disetujui',
                'message' => 'Pendaftaran anda tidak disetujui Admin',
                'useCatatan' => true
            ];
        } else if ($status == 'verified') {
            $response = [
                'color'   => 'green',
                'icon'    => 'check-circle',
                'title'   => 'Selamat',
                'message' => 'Pendaftaran anda disetujui Admin, Selamat Berjuang!',
            ];
        } else {
            $response = [
                'color'   => 'red',
                'icon'    => 'x-circle',
                'title'   => 'Mohon Maaf',
                'message' => 'Pendaftaran anda telah dibatalkan',
            ];
        }

        return $response;
    }
}
