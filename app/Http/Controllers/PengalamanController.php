<?php

namespace App\Http\Controllers;

use App\Pengalaman;
use Illuminate\Http\Request;

class PengalamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($pendaftaran_id)
    {
        return view('pengalaman.create', [
            'pendaftaran_id' => $pendaftaran_id
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $pendaftaran_id)
    {
        $pengalaman = new Pengalaman();
        $pengalaman->pendaftaran_id = $pendaftaran_id;
        $pengalaman->type = $request->type_pengalaman;
        $pengalaman->jabatan = $request->jabatan_pengalaman;
        $pengalaman->kegiatan = $request->deskripsi_pengalaman;
        $pengalaman->save();

        session()->flash('success', 'Data Pengalaman Berhasil Ditambah!');
        return redirect('/pendaftaran/'.$pendaftaran_id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pengalaman  $pengalaman
     * @return \Illuminate\Http\Response
     */
    public function show(Pengalaman $pengalaman)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pengalaman  $pengalaman
     * @return \Illuminate\Http\Response
     */
    public function edit($pendaftaran_id, $id)
    {
        $pengalaman = Pengalaman::find($id);
        return view('pengalaman.edit', [
            'pengalaman' => $pengalaman,
            'pendaftaran_id' => $pendaftaran_id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pengalaman  $pengalaman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $pendaftaran_id, $id)
    {
        $pengalaman = Pengalaman::find($id);
        $pengalaman->pendaftaran_id = $pendaftaran_id;
        $pengalaman->type = $request->type_pengalaman;
        $pengalaman->jabatan = $request->jabatan_pengalaman;
        $pengalaman->kegiatan = $request->deskripsi_pengalaman;
        $pengalaman->save();

        session()->flash('success', 'Data Pengalaman Berhasil Diubah!');
        return redirect('/pendaftaran/'.$pendaftaran_id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pengalaman  $pengalaman
     * @return \Illuminate\Http\Response
     */
    public function destroy($pendaftaran_id, $id)
    {
        Pengalaman::destroy($id);
        return response()->json([
            'message' => 'Data Pengalaman Berhasil Dihapus'
        ]);
    }
}
