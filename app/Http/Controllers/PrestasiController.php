<?php

namespace App\Http\Controllers;

use App\Prestasi;
use Illuminate\Http\Request;

class PrestasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($pendaftaran_id)
    {
        return view('prestasi.create', [
            'pendaftaran_id' => $pendaftaran_id
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $pendaftaran_id)
    {
        $prestasi = new Prestasi();
        $prestasi->pendaftaran_id = $pendaftaran_id;
        $prestasi->type = $request->type_prestasi;
        $prestasi->prestasi = $request->deskripsi_prestasi;
        $prestasi->save();

        session()->flash('success', 'Data Prestasi Berhasil Ditambah!');
        return redirect('/pendaftaran/'.$pendaftaran_id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Prestasi  $prestasi
     * @return \Illuminate\Http\Response
     */
    public function show(Prestasi $prestasi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Prestasi  $prestasi
     * @return \Illuminate\Http\Response
     */
    public function edit($pendaftaran_id, $id)
    {
        $prestasi = Prestasi::find($id);
        return view('prestasi.edit', [
            'prestasi' => $prestasi,
            'pendaftaran_id' => $pendaftaran_id
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Prestasi  $prestasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $pendaftaran_id, $id)
    {
        $prestasi = Prestasi::find($id);
        $prestasi->type = $request->type_prestasi;
        $prestasi->prestasi = $request->deskripsi_prestasi;
        $prestasi->save();

        session()->flash('success', 'Data Prestasi Berhasil Diubah!');
        return redirect('/pendaftaran/'.$pendaftaran_id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Prestasi  $prestasi
     * @return \Illuminate\Http\Response
     */
    public function destroy($pendaftaran_id, $id)
    {
        Prestasi::destroy($id);
        return response()->json([
            'message' => 'Data Prestasi Berhasil Dihapus'
        ]);
    }
}
