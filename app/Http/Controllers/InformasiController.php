<?php

namespace App\Http\Controllers;

use App\Informasi;
use Illuminate\Http\Request;

class InformasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $informasis  = Informasi::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $informasis->where('judul', 'like', '%' . $search . '%');
        }
        $informasis = $informasis->paginate(10);
        return view('informasi.index', [
            'informasis' => $informasis,
            'search' => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('informasi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $informasi = new Informasi();
        $informasi->type = $request->type;
        $informasi->judul = $request->judul;
        $file = '';
        if (isset($request->file)) {
            $file = $this->uploadGambar($request->file);
        }
        $informasi->file = $file;
        $informasi->deskripsi = $request->deskripsi;
        $informasi->save();

        session()->flash('success', 'Data Informasi Berhasil Ditambah!');
        return redirect('/informasi');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Informasi  $informasi
     * @return \Illuminate\Http\Response
     */
    public function show(Informasi $informasi)
    {
        return view('informasi.view', compact('informasi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Informasi  $informasi
     * @return \Illuminate\Http\Response
     */
    public function edit(Informasi $informasi)
    {
        return view('informasi.edit', compact('informasi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Informasi  $informasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Informasi $informasi)
    {
        $informasi->type = $request->type;
        $informasi->judul = $request->judul;
        if (isset($request->file)) {
            $file = $this->uploadGambar($request->file);
            $informasi->gambar = $file;
        }
        $informasi->deskripsi = $request->deskripsi;
        $informasi->save();

        session()->flash('success', 'Data Informasi Berhasil Diubah!');
        return redirect('/informasi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Informasi  $informasi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Informasi $informasi)
    {
        Informasi::destroy($informasi->id);
        return response()->json([
            'message' => 'Data Informasi Berhasil DiHapus'
        ]);
    }
}
