<?php

namespace App\Http\Controllers;

use PDF;
use App\Ormawa;
use App\Pendaftaran;
use App\Pengalaman;
use App\Prestasi;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;

class PendaftaranController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (auth()->user()->role != 'admin') {
            if (auth()->user()->pendaftaran != null) {
                return redirect()->route('pendaftaran.edit', auth()->user()->pendaftaran->id);
            } else {
                return redirect()->route('pendaftaran.create');
            }
        }
        
        $search = $request->search;
        $pendaftarans = Pendaftaran::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $pendaftarans->whereHas('user', function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%')->orWhere('email', 'like', '%' . $search . '%');
            });
        }
        $pendaftarans = $pendaftarans->paginate(10);

        return view('pendaftaran.index', [
            'pendaftarans' => $pendaftarans,
            'search' => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pendaftaran  $pendaftaran
     * @return \Illuminate\Http\Response
     */
    public function show(Pendaftaran $pendaftaran)
    {
        $ormawas = Ormawa::all();
        return view('pendaftaran.view', [
            'pendaftaran' => $pendaftaran,
            'ormawas' => $ormawas,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pendaftaran  $pendaftaran
     * @return \Illuminate\Http\Response
     */
    public function edit(Pendaftaran $pendaftaran)
    {
        $ormawa = Ormawa::all();
        $alert = $this->alertForStatus($pendaftaran->status);
        return view('pendaftaran.edit', [
            'pendaftaran' => $pendaftaran,
            'alert' => $alert,
            'ormawas' => $ormawa
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pendaftaran  $pendaftaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pendaftaran $pendaftaran)
    {
        if (isset($request->ajukan) && $request->ajukan == 1) {
            $validate = [
                'nim' => 'required',
                'type' => 'required',
                'nama_lengkap' => 'required',
                'nama_panggilan' => 'required',
                'ormawa_id' => 'required',
                'prodi' => 'required',
                'semester' => 'required',
                'tempat_lahir' => 'required',
                'tanggal_lahir' => 'required',
                'alamat' => 'required',
                'kota' => 'required',
                'agama' => 'required',
                'nama_orang_tua' => 'required',
                'orang_tua' => 'required',
                'no_telp_orang_tua' => 'required',
                'alamat_orang_tua' => 'required',
                'no_hp' => 'required',
                'hobi' => 'required',
                'yang_disuka' => 'required',
                'yang_tidak_disuka' => 'required',
                'kebiasaan_buruk' => 'required',
                'motto' => 'required',
                'tujuan' => 'required',
                'alasan' => 'required',
                'yang_akan_dilakukan' => 'required',
                'penjelasan_diri' => 'required',
                'motivasi' => 'required',
                'visi_misi' => 'required',
                'pendidikan_terakhir' => 'required',
                'type_sebagai'  => 'required',
            ];

            if (isset($request->checkbox_pekerjaan) && $request->checkbox_pekerjaan == 0) {
                $validate['pekerjaan'] = 'required';
                $validate['lokasi_kerja'] = 'required';
                $validate['jabatan_pekerjaan'] = 'required';
                $validate['tahun_kerja'] = 'required';
            }

            if (isset($request->checkbox_riwayat) && $request->checkbox_riwayat == 0) {
                $validate['sakit_pernah_diderita'] = 'required';
            }

            if (empty($pendaftaran->foto)) {
                $validate['foto'] = 'required';
            }

            if (empty($pendaftaran->surat_dukungan)) {
                $validate['surat_dukungan'] = 'required';
            }
            if (empty($pendaftaran->krs_terakhir)) {
                $validate['krs_terakhir'] = 'required';
            }
            if (empty($pendaftaran->foto_ktm)) {
                $validate['foto_ktm'] = 'required';
            }
            if (empty($pendaftaran->khs_terakhir)) {
                $validate['khs_terakhir'] = 'required';
            }
            if (empty($pendaftaran->sertif_kepengurusan)) {
                $validate['sertif_kepengurusan'] = 'required';
            }
            if (empty($pendaftaran->tanda_tangan)) {
                $validate['tanda_tangan'] = 'required';
            }
            if (empty($pendaftaran->tanda_tangan_orangtua)) {
                $validate['tanda_tangan_orangtua'] = 'required';
            }

            $request->validate($validate);
        } 
        
        $pendaftaran->user_id = auth()->user()->id;
        $pendaftaran->ormawa_id = $request->ormawa_id;
        $pendaftaran->nim = $request->nim;
        $pendaftaran->type = $request->type;
        if (isset($request->ajukan) && $request->ajukan == 1) {
            $pendaftaran->status = 'submit';
        }

        if (isset($request->ajukan_ulang) && $request->ajukan_ulang == 1) {
            $pendaftaran->status = 'resubmit';
        }

        if (isset($request->checkbox_pekerjaan) && $request->checkbox_pekerjaan == 0) {
            $pendaftaran->memilikiPekerjaan = true;
        } else {
            $pendaftaran->memilikiPekerjaan = false;
        }

        if (isset($request->checkbox_riwayat) && $request->checkbox_riwayat == 0) {
            $pendaftaran->memilikiRiwayat = true;
        } else {
            $pendaftaran->memilikiRiwayat = false;
        }
        $pendaftaran->nama_lengkap = $request->nama_lengkap;
        $pendaftaran->nama_panggilan = $request->nama_panggilan;
        $pendaftaran->prodi = $request->prodi;
        $pendaftaran->semester = $request->semester;
        $pendaftaran->tempat_lahir = $request->tempat_lahir;
        $pendaftaran->tanggal_lahir = $request->tanggal_lahir;
        $pendaftaran->alamat = $request->alamat;
        $pendaftaran->kota = $request->kota;
        $pendaftaran->agama = $request->agama;
        $pendaftaran->nama_orang_tua = $request->nama_orang_tua;
        $pendaftaran->orang_tua = $request->orang_tua;
        $pendaftaran->no_telp_orang_tua = $request->no_telp_orang_tua;
        $pendaftaran->alamat_orang_tua = $request->alamat_orang_tua;
        $pendaftaran->no_hp = $request->no_hp;
        $pendaftaran->hobi = $request->hobi;
        $pendaftaran->sakit_pernah_diderita = $request->sakit_pernah_diderita;
        if (isset($request->foto)) {
            $foto = $this->uploadGambar($request->foto);
            $pendaftaran->foto = $foto;
        }
        $pendaftaran->yang_disuka = $request->yang_disuka;
        $pendaftaran->yang_tidak_disuka = $request->yang_tidak_disuka;
        $pendaftaran->kebiasaan_buruk = $request->kebiasaan_buruk;
        $pendaftaran->motto = $request->motto;
        $pendaftaran->tujuan = $request->tujuan;
        $pendaftaran->alasan = $request->alasan;
        $pendaftaran->yang_akan_dilakukan = $request->yang_akan_dilakukan;
        $pendaftaran->penjelasan_diri = $request->penjelasan_diri;
        $pendaftaran->motivasi = $request->motivasi;
        $pendaftaran->visi_misi = $request->visi_misi;
        $pendaftaran->pendidikan_terakhir = $request->pendidikan_terakhir;
        $pendaftaran->pekerjaan = $request->pekerjaan;
        $pendaftaran->lokasi_kerja = $request->lokasi_kerja;
        $pendaftaran->jabatan_kerja = $request->jabatan_pekerjaan;
        $pendaftaran->tahun_kerja = $request->tahun_kerja;
        if (isset($request->surat_dukungan)) {
            $surat_dukungan = $this->uploadGambar($request->surat_dukungan);
            $pendaftaran->surat_dukungan = $surat_dukungan;
        }
        if (isset($request->krs_terakhir)) {
            $krs_terakhir = $this->uploadGambar($request->krs_terakhir);
            $pendaftaran->krs_terakhir = $krs_terakhir;
        }
        if (isset($request->foto_ktm)) {
            $foto_ktm = $this->uploadGambar($request->foto_ktm);
            $pendaftaran->foto_ktm = $foto_ktm;
        }
        if (isset($request->khs_terakhir)) {
            $khs_terakhir = $this->uploadGambar($request->khs_terakhir);
            $pendaftaran->khs_terakhir = $khs_terakhir;
        }
        if (isset($request->sertif_kepengurusan)) {
            $sertif_kepengurusan = $this->uploadGambar($request->sertif_kepengurusan);
            $pendaftaran->sertif_kepengurusan = $sertif_kepengurusan;
        }
        if (isset($request->tanda_tangan)) {
            $tanda_tangan = $this->uploadGambar($request->tanda_tangan);
            $pendaftaran->tanda_tangan = $tanda_tangan;
        }
        if (isset($request->tanda_tangan_orangtua)) {
            $tanda_tangan_orangtua = $this->uploadGambar($request->tanda_tangan_orangtua);
            $pendaftaran->tanda_tangan_orangtua = $tanda_tangan_orangtua;
        }
        $pendaftaran->type_sebagai = $request->type_sebagai;
        $pendaftaran->save();
        
        if (isset($request->ajukan) && $request->ajukan == 1) {
            $message = "🔵 <b>PENGGUNA TELAH MENGAJUKAN PENDAFTARAN</b> \n\n";
            $message .= "Nama Pengguna<b>\n- ".$pendaftaran->user->name."</b>\n";
            $message .= "Email <b>\n- ".$pendaftaran->user->email."</b>\n";
            $this->sendNotifTelegram($message);
        }

        if (isset($request->ajukan_ulang) && $request->ajukan_ulang == 1) {
            $message = "🔵 <b>PENGGUNA TELAH MENGAJUKAN ULANG PENDAFTARAN</b> \n\n";
            $message .= "Nama Pengguna<b>\n- ".$pendaftaran->user->name."</b>\n";
            $message .= "Email <b>\n- ".$pendaftaran->user->email."</b>\n";
            $this->sendNotifTelegram($message);
        }

        if ((isset($request->ajukan) && $request->ajukan == 1) || (isset($request->ajukan_ulang) && $request->ajukan_ulang == 1)) {
            session()->flash('success', 'Data Pendaftaran Berhasil Diajukan!');
        } else {
            session()->flash('success', 'Data Pendaftaran Berhasil Diubah!');
        }
        
        return redirect()->route('pendaftaran.edit', $pendaftaran->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pendaftaran  $pendaftaran
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pendaftaran $pendaftaran)
    {
        Pendaftaran::destroy($pendaftaran->id);
        return response()->json([
            'message' => 'Data Pendaftaran Berhasil  Di Hapus!'
        ]);
    }

    public function exportPdf($surat, $type, $id)
    {
        $pendaftaran = Pendaftaran::find($id);
        if ($surat == 'form') {
            $pdf = PDF::loadView('export.form_pendaftaran', [
                'pendaftaran' => $pendaftaran, 
                'type' => $type
            ]);
            return $pdf->stream('form_pendaftaran_'.$type.'.pdf');
        } elseif ($surat == 'izin') {
            $pdf = PDF::loadView('export.surat_izin_orang_tua', [
                'pendaftaran' => $pendaftaran, 
                'type' => $type
            ]);
            return $pdf->stream('surat_izin_'.$type.'.pdf');
        }
    }

    public function changeStatus(Request $request, $id)
    {
        $pendaftaran = Pendaftaran::find($request->id);
        $pendaftaran->status = $request->status;
        if (isset($request->message) && !empty($request->message)) {
            $pendaftaran->catatan = $request->message;
        }
        $pendaftaran->save();

        if ($request->status == 'unverified') {
            $message = "🟠 <b>ADMIN TELAH MENOLAK PENDAFTARAN </b> \n\n";
            $message .= "Nama Pengguna<b>\n- ".$pendaftaran->user->name."</b>\n";
            $message .= "Email <b>\n- ".$pendaftaran->user->email."</b>\n";
            $message .= "Catatan dari Admin <b>\n- ".$pendaftaran->catatan."</b>\n";
            $this->sendNotifTelegram($message);
        } else if ($request->status == 'verified'){
            $message = "🟢 <b>ADMIN TELAH MENYETUJUI PENDAFTARAN </b> \n\n";
            $message .= "Nama Pengguna<b>\n- ".$pendaftaran->user->name."</b>\n";
            $message .= "Email <b>\n- ".$pendaftaran->user->email."</b>\n";
            $this->sendNotifTelegram($message);
        } else if ($request->status == 'cancel') {
            $message = "🔴 <b>ADMIN TELAH MEMBATALKAN PENDAFTARAN </b> \n\n";
            $message .= "Nama Pengguna<b>\n- ".$pendaftaran->user->name."</b>\n";
            $message .= "Email <b>\n- ".$pendaftaran->user->email."</b>\n";
            $this->sendNotifTelegram($message);
        }

        return response()->json(['message' => 'Status berhasil diubah!']);
    }
}
