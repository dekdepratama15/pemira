<?php

namespace App\Http\Controllers;

use App\Pendaftaran;
use App\User;
use App\Ormawa;
use App\Informasi;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (auth()->user()->role == 'admin') {
            return view('dashboard', [
                'data' => $this->dataAll(),
                'user_new' => User::where('role', 'user')->orderBy('created_at', 'desc')->take(4)->get(),
                'status' => $this->dataStatus(),
                'type' => $this->dataType()
            ]);
        } else {
            return view('dashboard_user', [
                'data' => $this->dataAll(),
            ]);
        }
    }

    public function dataAll()
    {
        return [
            'pendaftaran' => Pendaftaran::all(),
            'user' => User::where('role', 'user')->get(),
            'ormawa' => Ormawa::all(),
            'informasi' => Informasi::all()
        ];
    }

    public function dataStatus()
    {
        $pendaftaran = Pendaftaran::orderBy('created_at', 'desc')->get();
        $pending = 0;
        $submit = 0;
        $resubmit = 0;
        $unverified = 0;
        $verified   = 0;
        $cancel  = 0;
        foreach ($pendaftaran as $key => $value) {
            if ($value->status == 'pending') {
                $pending++;
            } else if ($value->status == 'submit') {
                $submit++;
            } else if ($value->status == 'resubmit') {
                $resubmit++;
            } else if ($value->status == 'unverified') {
                $unverified++;
            } else if ($value->status == 'verified') {
                $verified++;
            } else {
                $cancel++;
            }
        }
        $result = [
            [
                'status' => 'Pending',
                'color' => '#6b7280',
                'color_name' => 'gray',
                'data' => $pending,
                'persentase' => count($pendaftaran) != 0 ? number_format(($pending / count($pendaftaran)) * 100, 0, '.' , '') : 0,
            ],
            [
                'status' => 'Submit',
                'color' => '#6366f1',
                'color_name' => 'indigo',
                'data' => $submit,
                'persentase' => count($pendaftaran) != 0 ? number_format(($submit / count($pendaftaran)) * 100, 0, '.' , '') : 0,
            ],
            [
                'status' => 'Resubmit',
                'color' => '#3b82f6',
                'color_name' => 'blue',
                'data' => $resubmit,
                'persentase' => count($pendaftaran) != 0 ? number_format(($resubmit / count($pendaftaran)) * 100, 0, '.' , '') : 0,
            ],
            [
                'status' => 'Unverified',
                'color' => '#eab308',
                'color_name' => 'yellow',
                'data' => $unverified,
                'persentase' => count($pendaftaran) != 0 ? number_format(($unverified / count($pendaftaran)) * 100, 0, '.' , '') : 0,
            ],
            [
                'status' => 'Verified',
                'color' => '#22c55e',
                'color_name' => 'green',
                'data' => $verified,
                'persentase' => count($pendaftaran) != 0 ? number_format(($verified / count($pendaftaran)) * 100, 0, '.' , '') : 0,
            ],
            [
                'status' => 'Cancel',
                'color' => '#ef4444',
                'color_name' => 'red',
                'data' => $cancel,
                'persentase' => count($pendaftaran) != 0 ? number_format(($cancel / count($pendaftaran)) * 100, 0, '.' , '') : 0,
            ],
        ];
        return $result;
    }

    public function getStatus()
    {
        $status = $this->dataStatus();
        
        foreach ($status as $key => $value) {
            $label[] = $value['status'];
            $dataset[] = $value['persentase'];
            $color[] = $value['color'];
        }

        
        return response()->json([
            'label' => $label,
            'dataset' => $dataset,
            'color' => $color,
        ]);
    }

    public function dataType()
    {
        $pendaftaran = Pendaftaran::all();
        $bem = 0;
        $balma = 0;
        $hima = 0;
        $belum = 0;
        foreach ($pendaftaran as $key => $value) {
            if ($value->type == 'Bem') {
                $bem++;
            } else if ($value->type == 'Balma') {
                $balma++;
            } else if ($value->type == 'Hima') {
                $hima++;
            } else {
                $belum++;
            }
        }
        return [$bem, $balma, $hima, $belum];
    }

    public function getType()
    {
        return response()->json($this->dataType());
    }
}
