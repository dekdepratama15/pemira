<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $users  = User::where('role', 'user');
        if (isset($search) && !empty($search)) {
            $users->where('name', 'like', '%' . $search . '%')->orWhere('email', 'like', '%' . $search . '%');
        }
        $users = $users->paginate(10);
        return view('user.index', [
            'users' => $users,
            'search' => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('user.view', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('profile');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find(auth()->user()->id);
        if (!empty($request->password) && !empty($request->confirmpassword)) {
            if ($request->password == $request->confirmpassword) {
                $user->password = bcrypt($request->password);
            } else {
                session()->flash('warning', 'Password Tidak Sama!');
                return redirect('/profile/' . $id);
            }
        }
        $profilfoto = '';
        if (isset($request->imgProfile)) {
            $profilfoto = $this->uploadGambar($request->imgProfile);
            $user->img_profile = $profilfoto;
        }
        $user->save();

        session()->flash('success', 'Berhasil Memperbaharui Profile');

        return redirect('/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return response()->json(['message' => 'Data User Berhasil Dihapus!']);
    }
}
