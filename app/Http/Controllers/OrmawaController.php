<?php

namespace App\Http\Controllers;

use App\Ormawa;
use Illuminate\Http\Request;

class OrmawaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $ormawas  = Ormawa::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $ormawas->where('ormawa', 'like', '%' . $search . '%');
        }
        $ormawas = $ormawas->paginate(10);
        return view('ormawa.index', [
            'ormawas' => $ormawas,
            'search' => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ormawa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ormawa = new Ormawa();
        $ormawa->ormawa = $request->ormawa;
        $ormawa->save();

        session()->flash('success', 'Data Ormawa Berhasil Ditambah!');
        return redirect('/ormawa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ormawa  $ormawa
     * @return \Illuminate\Http\Response
     */
    public function show(Ormawa $ormawa)
    {
        return view('ormawa.view', compact('ormawa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ormawa  $ormawa
     * @return \Illuminate\Http\Response
     */
    public function edit(Ormawa $ormawa)
    {
        return view('ormawa.edit', compact('ormawa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ormawa  $ormawa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ormawa $ormawa)
    {
        $ormawa->ormawa = $request->ormawa;
        $ormawa->save();

        session()->flash('success', 'Data Ormawa Berhasil Diubah!');
        return redirect('/ormawa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ormawa  $ormawa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ormawa $ormawa)
    {
        Ormawa::destroy($ormawa->id);
        return response()->json([
            'message' => 'Data Ormawa Berhasil Dihapus'
        ]);
    }
}
