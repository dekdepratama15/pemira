<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendaftaran extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ormawa()
    {
        return $this->belongsTo(Ormawa::class);
    }

    public function pengalamans()
    {
        return $this->hasMany(Pengalaman::class);
    }

    public function prestasis()
    {
        return $this->hasMany(Prestasi::class);
    }

}
